# `linear-sim`

> minimal linear 3D simulation

[Documentation](https://spearman.github.io/linear-sim/linear_sim/index.html)

This library is a small dynamic simulation with the following features:

- Static objects with fixed positions
- Dynamic objects with only linear (position) time derivatives
- Three axis-aligned bounding volumes for static or dynamic objects: `Sphere`,
  `Capsule` and `Cuboid` (AABB)
- Two unbounded volumes: `Orthant`s and `Halfspace`s for static objects
- Gravity force and user-created forces
- Continuous collision detection and response
