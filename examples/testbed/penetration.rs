use {math, glium, log, rand_xorshift};
use rand_xorshift::XorShiftRng;
use vec_map::VecMap;
use gl_utils::*;
use linear_sim::*;

use crate::program;

const DENSITY_MIN             : f64      =   150.0;   // balsa wood: 160 kg/m^3
const DENSITY_MAX             : f64      = 20000.0;   // gold: 19,320 kg/m^3
const POSITION_MIN            : [f64; 3] = [-5.0, -5.0,  2.0];
const POSITION_MAX            : [f64; 3] = [ 5.0,  5.0, 10.0];

const CAPSULE_RADIUS_MIN      : f64      =     0.05;  // 5cm
const CAPSULE_RADIUS_MAX      : f64      =     1.0;   // 1m
const CAPSULE_HALF_HEIGHT_MIN : f64      =     0.0;   // 0cm
const CAPSULE_HALF_HEIGHT_MAX : f64      =     3.0;   // 3m

const CUBOID_HALF_EXTENT_MIN  : f64      =     0.01;   // 1cm
const CUBOID_HALF_EXTENT_MAX  : f64      =     4.0;    // 4m

////////////////////////////////////////////////////////////////////////////////
//  capsule/capsule                                                           //
////////////////////////////////////////////////////////////////////////////////

pub fn init_capsule_capsule (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  use render::resource::default::draw3d;
  // reset initial render state and rng
  init (render_context, rng);

  // generate default debug grid vertex data
  let grid_vertex_data    = render::resource::Default::debug_grid_vertices();
  // generate initial capsule data
  let capsule_vertex_data = next_capsule_capsule_vertices (rng);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    v
  };
  // set per-instance data
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      meshes,
      .. Default::default()
    }
  );

  program::print_testbed_modes_prompt();
  println!("linear-sim penetration capsule v. capsule test");
  println!("  press 'Tab' to generate test cases");
}

pub fn draw_capsule_capsule (
  render_context      : &Render <render::resource::Default>,
  glium_frame         : &mut glium::Frame,
  center_indices_list : &glium::IndexBuffer <u32>
) {
  use glium::Surface;

  // default 3D draw pass
  render::Resource::draw_3d (render_context, glium_frame);

  let viewport =
    &render_context.viewports()[render::resource::default::MAIN_VIEWPORT];

  // draw parameters: viewport
  let draw_parameters_viewport = glium::DrawParameters {
    viewport: Some (*viewport.rect()),
    .. Default::default()
  };
  // draw parameters: depth test
  let draw_parameters_write_depth = glium::DrawParameters {
    depth: glium::Depth {
      test:  glium::DepthTest::IfLess,
      write: true,
      .. Default::default()
    },
    .. draw_parameters_viewport
  };
  // uniforms
  let (transform_mat_world_to_view, projection_mat_perspective) =
    viewport.camera3d().view_mats();
  let uniforms = glium::uniform!{
    uni_transform_mat_view:         transform_mat_world_to_view,
    uni_projection_mat_perspective: projection_mat_perspective,
    uni_color: color::rgba_u8_to_rgba_f32 (color::DEBUG_LIGHT_BLUE)
  };

  // draw lines between capsule centers
  glium_frame.draw (
    render_context.resource.draw3d.instance_vertices(),
    center_indices_list,
    &render_context.resource.shader_programs() [
      shader::ProgramId::WorldSpace3dUniColor as usize
    ],
    &uniforms,
    &draw_parameters_write_depth
  ).unwrap();
}

/// Generate the next pseudorandom intersecting sphere pair and update the
/// render context with the result
pub fn next_capsule_capsule (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  use render::resource::default::draw3d::MeshId;
  let capsule_vertex_data = next_capsule_capsule_vertices (rng);
  render_context.resource.draw3d.instance_vertices_mesh_write (
    MeshId::Capsule as usize, &capsule_vertex_data);
}

fn next_capsule_capsule_vertices (rng : &mut rand_xorshift::XorShiftRng)
  -> [vertex::Vert3dOrientationScaleColor; 4]
{
  let overlapping_capsules = generate_overlapping_capsule_capsule (rng);
  let resolved_capsules    =
    resolve_overlapping_capsule_capsule (&overlapping_capsules);
  let initial_capsules     = [
    overlapping_capsules[0].0.clone(), overlapping_capsules[1].0.clone()
  ];
  vertices_capsule_capsule (initial_capsules, resolved_capsules)
}

/// Generate a random pair of overlapping capsules with random mass
fn generate_overlapping_capsule_capsule (rng : &mut rand_xorshift::XorShiftRng)
  -> [(object::Static, f64); 2]
{
  use rand::Rng;
  use geometry::shape;
  use geometry::shape::Stereometric;

  let radius_a      = rng.gen_range (CAPSULE_RADIUS_MIN..CAPSULE_RADIUS_MAX);
  let radius_b      = rng.gen_range (CAPSULE_RADIUS_MIN..CAPSULE_RADIUS_MAX);
  let half_height_a = rng.gen_range (
    CAPSULE_HALF_HEIGHT_MIN..CAPSULE_HALF_HEIGHT_MAX);
  let half_height_b = rng.gen_range (
    CAPSULE_HALF_HEIGHT_MIN..CAPSULE_HALF_HEIGHT_MAX);
  let density_a     = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let density_b     = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let shape_a       = shape::Capsule::noisy (radius_a, half_height_a);
  let shape_b       = shape::Capsule::noisy (radius_b, half_height_b);
  let volume_a      = *shape_a.volume();
  let volume_b      = *shape_b.volume();
  let mass_a        = density_a * volume_a;
  let mass_b        = density_b * volume_b;
  let min_distance  = radius_a + radius_b;
  let min_dim       = f64::sqrt ((min_distance * min_distance) / 3.0);
  // restrict first capsule center to this AABB
  let initial_aabb  = geometry::Aabb3::<f64>::with_minmax (
    POSITION_MIN.into(), POSITION_MAX.into());
  let capsule_a     = object::Static {
    position:   component::Position (initial_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_a).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  let within_min : math::Point3 <f64> = capsule_a.position.0
    - math::Vector3::new (min_dim, min_dim, min_dim + half_height_a);
  let within_max : math::Point3 <f64> = capsule_a.position.0
    + math::Vector3::new (min_dim, min_dim, min_dim + half_height_a);
  let within_aabb  = geometry::Aabb3::<f64>::with_minmax (within_min, within_max);
  let capsule_b    = object::Static {
    position:   component::Position (within_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_b).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  [(capsule_a, mass_a), (capsule_b, mass_b)]
}

/// Pushes capsules apart based on relative masses to a distance of `0.5 *
/// CONTACT_DISTANCE`
fn resolve_overlapping_capsule_capsule (
  overlapping_capsules : &[(object::Static, f64); 2]
) -> [object::Static; 2] {
  log::trace!("resolve overlapping capsule pair...");

  let (ref capsule_a, ref mass_a) = overlapping_capsules[0];
  let (ref capsule_b, ref mass_b) = overlapping_capsules[1];

  let proximity        = collision::Proximity::query (capsule_a, capsule_b);
  log::trace!("  proximity: {:?}", proximity);
  debug_assert_eq!(proximity.relation(),
    collision::proximity::Relation::Intersect);

  let total_mass      = mass_a + mass_b;
  let relative_mass_a = mass_a / total_mass;
  let relative_mass_b = mass_b / total_mass;
  let move_a          = 1.0 - relative_mass_a;
  let move_b          = 1.0 - relative_mass_b;

  let resolved_capsule_a = object::Static {
    position: component::Position (capsule_a.position.0 +
      2.0 * move_a * proximity.half_axis +
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. capsule_a.clone()
  };
  let resolved_capsule_b = object::Static {
    position: component::Position (capsule_b.position.0 -
      2.0 * move_b * proximity.half_axis -
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. capsule_b.clone()
  };
  let resolved_proximity =
    collision::Proximity::query (&resolved_capsule_a, &resolved_capsule_b);

  debug_assert_eq!(
    resolved_proximity.relation(), collision::proximity::Relation::Contact);

  log::trace!("...resolve overlapping capsule pair");
  [
    resolved_capsule_a, resolved_capsule_b
  ]
}

fn vertices_capsule_capsule (
  overlapping_capsules : [object::Static; 2],
  resolved_capsules    : [object::Static; 2]
) -> [vertex::Vert3dOrientationScaleColor; 4] {
  [
    capsule_to_vertex (&overlapping_capsules[0], color::DEBUG_RED),
    capsule_to_vertex (&overlapping_capsules[1], color::DEBUG_YELLOW),
    capsule_to_vertex (&resolved_capsules[0],    color::DEBUG_GREEN),
    capsule_to_vertex (&resolved_capsules[1],    color::DEBUG_BLUE)
  ]
}

////////////////////////////////////////////////////////////////////////////////
//  cuboid/cuboid                                                             //
////////////////////////////////////////////////////////////////////////////////

pub fn init_cuboid_cuboid (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  use render::resource::default::draw3d;

  // reset initial render state and rng
  init (render_context, rng);

  // generate default debug grid vertex data
  let grid_vertex_data    = render::resource::Default::debug_grid_vertices();
  // generate initial capsule data
  let cuboid_vertex_data = next_cuboid_cuboid_vertices (rng);
  let aabb_lines = Some (&cuboid_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (1);
    assert!(v.insert (draw3d::MeshId::Grid as usize, &grid_vertex_data[..])
      .is_none());
    v
  };
  // set per-instance data
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  program::print_testbed_modes_prompt();
  println!("linear-sim penetration cuboid v. cuboid test");
  println!("  press 'Tab' to generate test cases");
}

pub fn draw_cuboid_cuboid (
  render_context      : &Render <render::resource::Default>,
  glium_frame         : &mut glium::Frame,
  center_indices_list : &glium::IndexBuffer <u32>
) {
  use glium::Surface;

  // default 3D draw pass
  render::Resource::draw_3d (render_context, glium_frame);

  let viewport =
    &render_context.viewports()[render::resource::default::MAIN_VIEWPORT];

  // draw parameters: viewport
  let draw_parameters_viewport = glium::DrawParameters {
    viewport: Some (*viewport.rect()),
    .. Default::default()
  };
  // draw parameters: depth test
  let draw_parameters_write_depth = glium::DrawParameters {
    depth: glium::Depth {
      test:  glium::DepthTest::IfLess,
      write: true,
      .. Default::default()
    },
    .. draw_parameters_viewport
  };
  // uniforms
  let (transform_mat_world_to_view, projection_mat_perspective) =
    viewport.camera3d().view_mats();
  let uniforms = glium::uniform!{
    uni_transform_mat_view:         transform_mat_world_to_view,
    uni_projection_mat_perspective: projection_mat_perspective,
    uni_color: color::rgba_u8_to_rgba_f32 (color::DEBUG_LIGHT_BLUE)
  };

  // draw lines between cuboid centers
  glium_frame.draw (
    render_context.resource.draw3d.instance_vertices(),
    center_indices_list,
    &render_context.resource.shader_programs() [
      shader::ProgramId::WorldSpace3dUniColor as usize
    ],
    &uniforms,
    &draw_parameters_write_depth
  ).unwrap();
}

/// Generate the next pseudorandom intersecting sphere pair and update the
/// render context with the result
pub fn next_cuboid_cuboid (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  let cuboid_vertex_data = next_cuboid_cuboid_vertices (rng);
  render_context.resource.draw3d
    .instance_vertices_aabb_lines_write (&cuboid_vertex_data);
}

fn next_cuboid_cuboid_vertices (rng : &mut rand_xorshift::XorShiftRng)
  -> [vertex::Vert3dOrientationScaleColor; 4]
{
  let overlapping_cuboids = generate_overlapping_cuboid_cuboid (rng);
  let resolved_cuboids    =
    resolve_overlapping_cuboid_cuboid (&overlapping_cuboids);
  let initial_cuboids     = [
    overlapping_cuboids[0].0.clone(), overlapping_cuboids[1].0.clone()
  ];
  vertices_cuboid_cuboid (initial_cuboids, resolved_cuboids)
}

/// Generate a random pair of overlapping cuboids with random mass
fn generate_overlapping_cuboid_cuboid (rng : &mut rand_xorshift::XorShiftRng)
  -> [(object::Static, f64); 2]
{
  use rand::Rng;
  use geometry::shape;
  use geometry::shape::Stereometric;

  let half_extents_a = [
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX)
  ];
  let half_extents_b = [
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX)
  ];
  let density_a      = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let density_b      = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let shape_a        = shape::Cuboid::noisy (half_extents_a);
  let shape_b        = shape::Cuboid::noisy (half_extents_b);
  let volume_a       = *shape_a.volume();
  let volume_b       = *shape_b.volume();
  let mass_a         = density_a * volume_a;
  let mass_b         = density_b * volume_b;
  // minimum distance along each axis
  let min_distance   = math::Vector3::new (
    half_extents_a[0] + half_extents_b[0],
    half_extents_a[1] + half_extents_b[1],
    half_extents_a[2] + half_extents_b[2]
  );
  // restrict first cuboid center to this AABB
  let initial_aabb   = geometry::Aabb3::<f64>::with_minmax (
    POSITION_MIN.into(), POSITION_MAX.into());
  let cuboid_a       = object::Static {
    position:   component::Position (initial_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_a).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  let within_min : math::Point3 <f64> = cuboid_a.position.0 - min_distance;
  let within_max : math::Point3 <f64> = cuboid_a.position.0 + min_distance;
  let within_aabb = geometry::Aabb3::<f64>::with_minmax (within_min, within_max);
  let cuboid_b    = object::Static {
    position:   component::Position (within_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_b).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  [(cuboid_a, mass_a), (cuboid_b, mass_b)]
}

/// Pushes cuboids apart based on relative masses to a distance of `0.5 *
/// CONTACT_DISTANCE`
fn resolve_overlapping_cuboid_cuboid (
  overlapping_cuboids : &[(object::Static, f64); 2]
) -> [object::Static; 2] {
  log::trace!("resolve overlapping cuboid pair...");

  let (ref cuboid_a, ref mass_a) = overlapping_cuboids[0];
  let (ref cuboid_b, ref mass_b) = overlapping_cuboids[1];

  let proximity        = collision::Proximity::query (cuboid_a, cuboid_b);
  log::trace!("  proximity: {:?}", proximity);
  debug_assert_eq!(
    proximity.relation(), collision::proximity::Relation::Intersect);

  let total_mass       = mass_a + mass_b;
  let relative_mass_a  = mass_a / total_mass;
  let relative_mass_b  = mass_b / total_mass;
  let move_a           = 1.0 - relative_mass_a;
  let move_b           = 1.0 - relative_mass_b;

  let resolved_cuboid_a = object::Static {
    position: component::Position (cuboid_a.position.0 +
      2.0 * move_a * proximity.half_axis +
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. cuboid_a.clone()
  };
  let resolved_cuboid_b = object::Static {
    position: component::Position (cuboid_b.position.0 -
      2.0 * move_b * proximity.half_axis -
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. cuboid_b.clone()
  };
  let resolved_proximity =
    collision::Proximity::query (&resolved_cuboid_a, &resolved_cuboid_b);

  debug_assert_eq!(
    resolved_proximity.relation(),
    collision::proximity::Relation::Contact
  );
  log::trace!("...resolve overlapping cuboid pair");
  [
    resolved_cuboid_a, resolved_cuboid_b
  ]
}

fn vertices_cuboid_cuboid (
  overlapping_cuboids : [object::Static; 2],
  resolved_cuboids    : [object::Static; 2]
) -> [vertex::Vert3dOrientationScaleColor; 4] {
  [
    cuboid_to_vertex (&overlapping_cuboids[0], color::DEBUG_RED),
    cuboid_to_vertex (&overlapping_cuboids[1], color::DEBUG_YELLOW),
    cuboid_to_vertex (&resolved_cuboids[0],    color::DEBUG_GREEN),
    cuboid_to_vertex (&resolved_cuboids[1],    color::DEBUG_BLUE)
  ]
}

////////////////////////////////////////////////////////////////////////////////
//  capsule/cuboid                                                            //
////////////////////////////////////////////////////////////////////////////////

pub fn init_capsule_cuboid (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  use render::resource::default::draw3d;

  // reset initial render state and rng
  init (render_context, rng);

  // generate default debug grid vertex data
  let grid_vertex_data    = render::resource::Default::debug_grid_vertices();
  // generate initial capsule data
  let (capsule_vertex_data, cuboid_vertex_data) =
    next_capsule_cuboid_vertices (rng);
  let aabb_lines = Some (&cuboid_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    v
  };
  // set per-instance data
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  program::print_testbed_modes_prompt();
  println!("linear-sim penetration capsule v. cuboid test");
  println!("  press 'Tab' to generate test cases");
}

pub fn draw_capsule_cuboid (
  render_context      : &Render <render::resource::Default>,
  glium_frame         : &mut glium::Frame,
  center_indices_list : &glium::IndexBuffer <u32>,
) {
  use glium::Surface;

  // default 3D draw pass
  render::Resource::draw_3d (render_context, glium_frame);

  let viewport =
    &render_context.viewports()[render::resource::default::MAIN_VIEWPORT];

  // draw parameters: viewport
  let draw_parameters_viewport = glium::DrawParameters {
    viewport: Some (*viewport.rect()),
    .. Default::default()
  };
  // draw parameters: depth test
  let draw_parameters_write_depth = glium::DrawParameters {
    depth: glium::Depth {
      test:  glium::DepthTest::IfLess,
      write: true,
      .. Default::default()
    },
    .. draw_parameters_viewport
  };
  // uniforms
  let (transform_mat_world_to_view, projection_mat_perspective) =
    viewport.camera3d().view_mats();
  let uniforms = glium::uniform!{
    uni_transform_mat_view:         transform_mat_world_to_view,
    uni_projection_mat_perspective: projection_mat_perspective,
    uni_color: color::rgba_u8_to_rgba_f32 (color::DEBUG_LIGHT_BLUE)
  };

  // draw lines between capsule centers
  glium_frame.draw (
    render_context.resource.draw3d.instance_vertices(),
    center_indices_list,
    &render_context.resource.shader_programs() [
      shader::ProgramId::WorldSpace3dUniColor as usize
    ],
    &uniforms,
    &draw_parameters_write_depth
  ).unwrap();
}

/// Generate the next pseudorandom intersecting sphere pair and update the
/// render context with the result
pub fn next_capsule_cuboid (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) {
  use render::resource::default::draw3d::MeshId;
  let (capsule_vertex_data, cuboid_vertex_data) =
    next_capsule_cuboid_vertices (rng);
  render_context.resource.draw3d.instance_vertices_mesh_write (
    MeshId::Capsule as usize, &capsule_vertex_data);
  render_context.resource.draw3d
    .instance_vertices_aabb_lines_write (&cuboid_vertex_data);
}

fn next_capsule_cuboid_vertices (rng : &mut rand_xorshift::XorShiftRng)
  -> (
  [vertex::Vert3dOrientationScaleColor; 2],
  [vertex::Vert3dOrientationScaleColor; 2]
) {
  let overlapping_capsule_cuboid = generate_overlapping_capsule_cuboid (rng);
  let resolved_capsule_cuboid    =
    resolve_overlapping_capsule_cuboid (&overlapping_capsule_cuboid);
  let initial_capsule_cuboid     = [
    overlapping_capsule_cuboid[0].0.clone(),
    overlapping_capsule_cuboid[1].0.clone()
  ];
  vertices_capsule_cuboid (initial_capsule_cuboid, resolved_capsule_cuboid)
}

/// Generate a random capsule and cuboid
fn generate_overlapping_capsule_cuboid (rng : &mut rand_xorshift::XorShiftRng)
  -> [(object::Static, f64); 2]
{
  use rand::Rng;
  use geometry::shape;
  use geometry::shape::Stereometric;

  let radius_a      = rng.gen_range (CAPSULE_RADIUS_MIN..CAPSULE_RADIUS_MAX);
  let half_height_a = rng.gen_range (
    CAPSULE_HALF_HEIGHT_MIN..CAPSULE_HALF_HEIGHT_MAX);
  let half_extents_b = [
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX),
    rng.gen_range (CUBOID_HALF_EXTENT_MIN..CUBOID_HALF_EXTENT_MAX)
  ].into();
  let density_a     = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let density_b     = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
  let shape_a       = shape::Capsule::noisy (radius_a, half_height_a);
  let shape_b       = shape::Cuboid::noisy (half_extents_b);
  let volume_a      = *shape_a.volume();
  let volume_b      = *shape_b.volume();
  let mass_a        = density_a * volume_a;
  let mass_b        = density_b * volume_b;
  let min_distance  = radius_a;
  let min_dim       = f64::sqrt ((min_distance * min_distance) / 3.0);
  // restrict capsule center to this AABB
  let initial_aabb  = geometry::Aabb3::<f64>::with_minmax (
    POSITION_MIN.into(), POSITION_MAX.into());
  let capsule_a     = object::Static {
    position:   component::Position (initial_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_a).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  let within_min : math::Point3 <f64> = capsule_a.position.0
    - math::Vector3::new (min_dim, min_dim, min_dim + half_height_a);
  let within_max : math::Point3 <f64> = capsule_a.position.0
    + math::Vector3::new (min_dim, min_dim, min_dim + half_height_a);
  let within_aabb = geometry::Aabb3::<f64>::with_minmax (within_min, within_max);
  let cuboid_b    = object::Static {
    position:   component::Position (within_aabb.rand_point (rng)),
    bound:      component::Bound    (shape::Bounded::from (shape_b).into()),
    material:   component::MATERIAL_STONE,
    collidable: true
  };
  [(capsule_a, mass_a), (cuboid_b, mass_b)]
}

/// Pushes capsules apart based on relative masses to a distance of `0.5 *
/// CONTACT_DISTANCE`
fn resolve_overlapping_capsule_cuboid (
  overlapping_capsule_cuboid : &[(object::Static, f64); 2]
) -> [object::Static; 2] {
  log::trace!("resolve overlapping capsule pair...");

  let (ref capsule_a, ref mass_a) = overlapping_capsule_cuboid[0];
  let (ref cuboid_b,  ref mass_b) = overlapping_capsule_cuboid[1];

  let proximity        = collision::Proximity::query (capsule_a, cuboid_b);
  log::trace!("  proximity: {:?}", proximity);
  debug_assert_eq!(
    proximity.relation(), collision::proximity::Relation::Intersect);

  let total_mass       = mass_a + mass_b;
  let relative_mass_a  = mass_a / total_mass;
  let relative_mass_b  = mass_b / total_mass;
  let move_a           = 1.0 - relative_mass_a;
  let move_b           = 1.0 - relative_mass_b;

  let resolved_capsule_a = object::Static {
    position: component::Position (capsule_a.position.0 +
      2.0 * move_a * proximity.half_axis +
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. capsule_a.clone()
  };
  let resolved_cuboid_b = object::Static {
    position: component::Position (cuboid_b.position.0 -
      2.0 * move_b * proximity.half_axis -
      0.25 * collision::CONTACT_DISTANCE * *proximity.normal),
    .. cuboid_b.clone()
  };
  let resolved_proximity =
    collision::Proximity::query (&resolved_capsule_a, &resolved_cuboid_b);

  debug_assert_eq!(
    resolved_proximity.relation(), collision::proximity::Relation::Contact);

  log::trace!("...resolve overlapping capsule pair");
  [ resolved_capsule_a, resolved_cuboid_b ]
}

fn vertices_capsule_cuboid (
  overlapping_capsule_cuboid : [object::Static; 2],
  resolved_capsule_cuboid    : [object::Static; 2]
) -> (
  [vertex::Vert3dOrientationScaleColor; 2],
  [vertex::Vert3dOrientationScaleColor; 2]
) {
  ([
    capsule_to_vertex (&overlapping_capsule_cuboid[0], color::DEBUG_RED),
    capsule_to_vertex (&resolved_capsule_cuboid[0],    color::DEBUG_GREEN)
  ], [
    cuboid_to_vertex  (&overlapping_capsule_cuboid[1], color::DEBUG_YELLOW),
    cuboid_to_vertex  (&resolved_capsule_cuboid[1],    color::DEBUG_BLUE)
  ])
}

////////////////////////////////////////////////////////////////////////////////
//  utility functions                                                         //
////////////////////////////////////////////////////////////////////////////////

fn init (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut XorShiftRng
) {
  use rand::SeedableRng;
  // reset initial render state
  program::reset_render_context (render_context);
  const HALF_GRID_DIMS : f32 = 0.5 *
    render::resource::default::draw3d::MESH_GRID_DIMS as f32;
  render_context.camera3d_position_set ([0.0, -2.0*HALF_GRID_DIMS, 4.0].into());
  // reset initial seeded rng state
  *rng = XorShiftRng::seed_from_u64 (0);
}

fn capsule_to_vertex (object : &object::Static, color : [u8; 4])
  -> vertex::Vert3dOrientationScaleColor
{
  use geometry::shape;

  let component::Position (ref position) = object.position;
  let capsule = {
    match object.bound {
      component::Bound (shape::Variant::Bounded (
        shape::Bounded::Capsule (ref capsule)
      )) => capsule,
      _  => panic!("capsule to vertex called on a non-capsule object: {:?}",
        object)
    }
  };
  vertex::Vert3dOrientationScaleColor {
    position:    position.0.numcast().unwrap().into_array(),
    orientation: math::Matrix3::identity().into_col_arrays(),
    scale:       [
      *capsule.radius as f32,
      *capsule.radius as f32,
      *(capsule.radius + capsule.half_height) as f32
    ],
    color:       color::rgba_u8_to_rgba_f32 (color)
  }
}

fn cuboid_to_vertex (object : &object::Static, color : [u8; 4])
  -> vertex::Vert3dOrientationScaleColor
{
  use geometry::shape;

  let component::Position (ref position) = object.position;
  let cuboid = {
    match object.bound {
      component::Bound (shape::Variant::Bounded (
        shape::Bounded::Cuboid (ref cuboid)
      )) => cuboid,
      _  => panic!("cuboid to vertex called on a non-cuboid object: {:?}",
        object)
    }
  };
  vertex::Vert3dOrientationScaleColor {
    position:    position.0.numcast().unwrap().into_array(),
    orientation: math::Matrix3::identity().into_col_arrays(),
    scale:       [
      *cuboid.half_extent_x as f32,
      *cuboid.half_extent_y as f32,
      *cuboid.half_extent_z as f32
    ],
    color:       color::rgba_u8_to_rgba_f32 (color)
  }
}
