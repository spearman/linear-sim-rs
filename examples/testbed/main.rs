#![warn(unused_extern_crates)]

use std;
use env_logger;
use log;

use macro_machines;

use gl_utils;
use gl_utils::*;

use linear_sim;
use linear_sim::*;

pub mod program;      // program state machine

pub mod penetration;  // penetration resolution demos
pub mod simulation;   // simulation demos

const LOG_LEVEL    : log::LevelFilter = log::LevelFilter::Info;
const LOG_FILENAME : &'static str     = "testbed.log";

const INITIAL_2D_ZOOM     : f32      = 1.0;
const INITIAL_3D_POSITION : [f32; 3] = [0.0, 0.0, 2.0];
const INITIAL_CLEAR_COLOR : [f32; 4] = [0.0, 0.0, 0.0, 1.0];

/// Halt simulation after this step
//const DEBUG_MAX_STEP : Option <u64> = Some (60);
const DEBUG_MAX_STEP : Option <u64> = None;

fn main() {
  println!("linear-sim testbed...");

  // initialize logger
  let log_file = Box::new (std::fs::File::create (LOG_FILENAME).unwrap());
  env_logger::Builder::new()
    .filter_level (LOG_LEVEL)
    .parse_default_env()
    .target (env_logger::Target::Pipe (log_file))
    .init();

  // initialize glium
  log::info!("initializing glium...");
  let (mut event_loop, window, _gl_config, glium_display) =
    init::glium_init_gl33core ("linear-sim testbed window");

  // output some diagnostics
  { // print glium opengl context info
    use glium::backend::Facade;
    let context = glium_display.get_context();
    log::info!("opengl version: {:?}", context.get_opengl_version());
    log::info!("opengl profile: {:?}", context.get_opengl_profile());
    log::info!("supported glsl version: {:?}",
      context.get_supported_glsl_version());
    log::info!("free video memory: {}MB",
      context.get_free_video_memory().unwrap()/1_000_000);
  }
  // report size information
  program::Testbed::report_sizes();
  { // create dotfile with testbed state machine
    use std::io::Write;
    use macro_machines::MachineDotfile;
    let mut f = std::fs::File::create ("testbed.dot").unwrap();
    f.write_all (program::Testbed::dotfile_hide_actions().as_bytes()).unwrap();
  }
  object::report_sizes();
  system::report_sizes();
  collision::report_sizes();

  // create testbed state machine
  let mut testbed = {
    // acquire and setup render context
    let render_context = Render::new (glium_display, window);
    program::Testbed::new (
      program::ExtendedState::new (Some (render_context), None).unwrap())
  };

  // welcome
  println!("Welcome");
  println!("  Press 'C' to show current camera state");

  // main loop
  let mut fps     = 0;
  let mut last_t  = std::time::SystemTime::now();
  let mut running = true;   // exit condition for main loop when false
  while running {
    use winit::platform::pump_events::EventLoopExtPumpEvents;
    // TODO: review
    // pump winit events
    //
    // - window events includes input events received by the window
    // - device events are received independent of window focus
    // there may be differences in the events received depending on platform:
    // - on Linux keyboard input is received as both a device and window event
    // - on Windows only mouse motion device events are received, all other
    //   input is received as window events
    // here we just handle input contained in window events
    event_loop.pump_events (Some (std::time::Duration::ZERO), |event, _| {
      use winit::event::{self, Event};
      use winit::keyboard;
      //
      // program handle event
      //
      if let Some (program_event) = match event {
        // window event
        Event::WindowEvent { ref event, .. } => match *event {
          // window event: key pressed
          event::WindowEvent::KeyboardInput {
            event: event::KeyEvent {
              state: event::ElementState::Pressed, physical_key, ..
            },
            ..
          } => match physical_key {
            keyboard::PhysicalKey::Code (key_code) => match key_code {
              // C: print camera state
              keyboard::KeyCode::KeyC => {
                log::info!("current camera: {:?}",
                  testbed.as_mut().render_context
                    .viewports()[render::resource::default::MAIN_VIEWPORT]
                    .camera3d()
                );
                None
              }

              // 0-9: switch to mode
              keyboard::KeyCode::Digit1 =>
                if testbed.state_id() != program::StateId::SimulationDropTest {
                  Some (program::EventParams::ToSimulationDropTest{}.into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit2 =>
                if testbed.state_id() !=
                  program::StateId::PenetrationCapsuleCapsule
                {
                  Some (program::EventParams::ToPenetrationCapsuleCapsule{}
                    .into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit3 =>
                if testbed.state_id() !=
                  program::StateId::PenetrationCuboidCuboid
                {
                  Some (program::EventParams::ToPenetrationCuboidCuboid{}.into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit4 =>
                if testbed.state_id() !=
                  program::StateId::PenetrationCapsuleCuboid
                {
                  Some (program::EventParams::ToPenetrationCapsuleCuboid{}
                    .into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit5 =>
                if testbed.state_id() !=
                  program::StateId::SimulationCollideCapsuleCapsule
                {
                  Some (program::EventParams::ToSimulationCollideCapsuleCapsule{}
                    .into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit6 =>
                if testbed.state_id() != program::StateId::SimulationStackTest {
                  Some (program::EventParams::ToSimulationStackTest{}.into())
                } else {
                  None
                }
              keyboard::KeyCode::Digit7 =>
                if testbed.state_id() !=
                  program::StateId::SimulationBallPitTest
                {
                  Some (program::EventParams::ToSimulationBallPitTest{}.into())
                } else {
                  None
                }
              // Tab key generate new collision or penetration examples
              keyboard::KeyCode::Tab => match testbed.state_id() {
                program::StateId::PenetrationCapsuleCapsule => Some (
                  program::EventParams::NextPenetrationCapsuleCapsule{}.into()
                ),
                program::StateId::PenetrationCuboidCuboid => Some (
                  program::EventParams::NextPenetrationCuboidCuboid{}.into()
                ),
                program::StateId::PenetrationCapsuleCuboid => Some (
                  program::EventParams::NextPenetrationCapsuleCuboid{}.into()
                ),
                program::StateId::SimulationCollideCapsuleCapsule => Some (
                  program::EventParams::NextSimulationCollideCapsuleCapsule{}
                    .into()
                ),
                program::StateId::SimulationBallPitTest => Some (
                  program::EventParams::NextSimulationBallPitTest{}.into()
                ),
                _ => None
              }
              // P: toggle simulation pause/resume
              keyboard::KeyCode::KeyP => match testbed.state_data() {
                program::StateData::SimulationDropTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::ResumeSimulationDropTest{}
                      .into())
                  } else {
                    Some (program::EventParams::PauseSimulationDropTest{}.into())
                  }
                program::StateData::SimulationCollideCapsuleCapsule {
                  playback, ..
                } => if playback.paused {
                  Some (
                    program::EventParams::ResumeSimulationCollideCapsuleCapsule{}
                      .into())
                } else {
                  Some (
                    program::EventParams::PauseSimulationCollideCapsuleCapsule{}
                      .into())
                }
                program::StateData::SimulationStackTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::ResumeSimulationStackTest{}
                      .into())
                  } else {
                    Some (program::EventParams::PauseSimulationStackTest{}
                      .into())
                  }
                program::StateData::SimulationBallPitTest { playback, ..  } =>
                  if playback.paused {
                    Some (
                      program::EventParams::ResumeSimulationBallPitTest{}.into())
                  } else {
                    Some (
                      program::EventParams::PauseSimulationBallPitTest{}.into())
                  }
                _ => None
              }
              // step (or pause if not paused)
              keyboard::KeyCode::Period => match testbed.state_data() {
                program::StateData::SimulationDropTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::StepSimulationDropTest{}.into())
                  } else {
                    Some (program::EventParams::PauseSimulationDropTest{}.into())
                  }
                program::StateData::SimulationCollideCapsuleCapsule {
                  playback, ..
                } => if playback.paused {
                  Some (
                    program::EventParams::StepSimulationCollideCapsuleCapsule{}
                      .into())
                } else {
                  Some (
                    program::EventParams::PauseSimulationCollideCapsuleCapsule{}
                      .into())
                }
                program::StateData::SimulationStackTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::StepSimulationStackTest{}.into())
                  } else {
                    Some (program::EventParams::PauseSimulationStackTest{}
                      .into())
                  }
                program::StateData::SimulationBallPitTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::StepSimulationBallPitTest{}
                      .into())
                  } else {
                    Some (program::EventParams::PauseSimulationBallPitTest{}
                      .into())
                  }
                _ => None
              }
              // reverse step (or pause if not paused)
              keyboard::KeyCode::Comma => match testbed.state_data() {
                program::StateData::SimulationDropTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::StepReverseSimulationDropTest{}
                      .into())
                  } else {
                    Some (program::EventParams::PauseSimulationDropTest{}.into())
                  }
                program::StateData::SimulationCollideCapsuleCapsule {
                  playback, ..
                } => if playback.paused {
                  Some (
                    program::EventParams::StepReverseSimulationCollideCapsuleCapsule{}
                      .into())
                } else {
                  Some (
                    program::EventParams::PauseSimulationCollideCapsuleCapsule{}
                      .into())
                }
                program::StateData::SimulationStackTest { playback, ..  } =>
                  if playback.paused {
                    Some (program::EventParams::StepReverseSimulationStackTest{}
                      .into())
                  } else {
                    Some (
                      program::EventParams::PauseSimulationStackTest{}.into())
                  }
                program::StateData::SimulationBallPitTest { playback, ..  } =>
                  if playback.paused {
                    Some (
                      program::EventParams::StepReverseSimulationBallPitTest{}
                        .into())
                  } else {
                    Some (program::EventParams::PauseSimulationBallPitTest{}
                      .into())
                  }
                _ => None
              }
              // restart simulation
              keyboard::KeyCode::Backspace => match testbed.state_id() {
                program::StateId::SimulationDropTest => Some (
                  program::EventParams::RestartSimulationDropTest{}.into()),
                program::StateId::SimulationCollideCapsuleCapsule => Some (
                  program::EventParams::RestartSimulationCollideCapsuleCapsule{}
                    .into()),
                program::StateId::SimulationBallPitTest => Some (
                  program::EventParams::RestartSimulationBallPitTest{}.into()),
                program::StateId::SimulationStackTest => Some (
                  program::EventParams::RestartSimulationStackTest{}.into()),
                _ => None
              }
              _ => None   // unhandled key codes
            }
            keyboard::PhysicalKey::Unidentified (native_key_code) => {
              log::warn!("keyboard input: unidentified physical keycode: {:?}",
                native_key_code);
              None
            }
          }
          _ => None   // unhandled window events
        } // end match window event
        _ => None   // unhandled winit events: device events and others
      } { // end match winit event
        testbed.handle_event (program_event).unwrap();
      } else {
        // if not handled, forward to the default demo handler
        let mut mouse_button_event = None;
        let mut mouse_position = (0.0, 0.0);
        testbed.as_mut().render_context.demo_handle_winit_event (
          &mut running, event, &mut mouse_position, &mut mouse_button_event)
      }
    });

    // updates
    let mut debug_break_max_step = |testbed : &mut program::Testbed| {
      let system = match testbed.state_data() {
        program::StateData::SimulationDropTest { ref simulation, .. }  |
        program::StateData::SimulationStackTest { ref simulation, .. } |
        program::StateData::SimulationBallPitTest {
          simulation_generation: (ref simulation, _), ..
        } |
        program::StateData::SimulationCollideCapsuleCapsule {
          simulation_generation: (ref simulation, _), ..
        } => &simulation.system,
        _ => unreachable!("state id should be checked prior to this match")
      };
      if let Some (max_step) = DEBUG_MAX_STEP {
        if system.step() >= max_step {
          if system.step() > max_step {
            log::warn!("system step ({}) > max_step ({})",
              system.step(), max_step);
          }
          running = false;
        }
      }
    };
    match testbed.state_id() {
      program::StateId::SimulationDropTest => {
        testbed.handle_event (
          program::EventParams::UpdateSimulationDropTest{}.into()
        ).unwrap();

        // DEBUG: break main loop if max step is specified
        if cfg!(debug_assertions) {
          debug_break_max_step (&mut testbed);
        }
      }
      program::StateId::SimulationCollideCapsuleCapsule => {
        testbed.handle_event (
          program::EventParams::UpdateSimulationCollideCapsuleCapsule{}.into()
        ).unwrap();

        // DEBUG: break main loop if max step is specified
        if cfg!(debug_assertions) {
          debug_break_max_step (&mut testbed);
        }
      }
      program::StateId::SimulationStackTest => {
        testbed.handle_event (
          program::EventParams::UpdateSimulationStackTest{}.into()
        ).unwrap();

        // DEBUG: break main loop if max step is specified
        if cfg!(debug_assertions) {
          debug_break_max_step (&mut testbed);
        }
      }
      program::StateId::SimulationBallPitTest => {
        testbed.handle_event (
          program::EventParams::UpdateSimulationBallPitTest{}.into()
        ).unwrap();

        // DEBUG: break main loop if max step is specified
        if cfg!(debug_assertions) {
          debug_break_max_step (&mut testbed);
        }
      }
      _ => {}
    }

    // draw frame
    use glium::Surface;
    let mut glium_frame = testbed.as_mut().render_context.glium_display.draw();
    // clear frame
    glium_frame.clear_all (
      ( testbed.as_mut().render_context.clear_color[0],
        testbed.as_mut().render_context.clear_color[1],
        testbed.as_mut().render_context.clear_color[2],
        testbed.as_mut().render_context.clear_color[3]
      ),
      1.0, 0
    );

    // draw 3d
    match testbed.state_id() {
      program::StateId::PenetrationCapsuleCapsule => {
        let e = program::EventParams::DrawPenetrationCapsuleCapsule {
          glium_frame: &mut glium_frame
        }.into();
        testbed.handle_event (e).unwrap();
      }
      program::StateId::PenetrationCuboidCuboid => {
        let e = program::EventParams::DrawPenetrationCuboidCuboid {
          glium_frame: &mut glium_frame
        }.into();
        testbed.handle_event (e).unwrap();
      }
      program::StateId::PenetrationCapsuleCuboid => {
        let e = program::EventParams::DrawPenetrationCapsuleCuboid {
          glium_frame: &mut glium_frame
        }.into();
        testbed.handle_event (e).unwrap();
      }
      _ => render::Resource::draw_3d (
        &testbed.as_mut().render_context, &mut glium_frame)
    }

    // draw 2d
    render::Resource::draw_2d (
      &testbed.as_ref().render_context, &mut glium_frame);

    // swap buffers
    glium_frame.finish().unwrap();

    // fps
    let t = std::time::SystemTime::now();
    if 1 <= t.duration_since (last_t).unwrap().as_secs() {
      log::info!("fps: {}", fps);
      last_t = t;
      fps = 0;
    } else {
      fps += 1;
    }

  } // end main loop

  println!("...linear-sim testbed");
}
