//! Program state machine

use {glium, log, rand_xorshift};

use rand_xorshift::XorShiftRng;

use macro_machines::def_machine_nodefault;
use gl_utils::*;

use crate::{penetration, simulation};

use simulation::Simulation;

def_machine_nodefault!{
  Testbed (
    render_context : Render <render::resource::Default>,
    rng            : XorShiftRng = rand::SeedableRng::seed_from_u64 (0)
  ) @ testbed {
////////////////////////////////////////////////////////////////////////////////
//  states                                                                    //
////////////////////////////////////////////////////////////////////////////////
    STATES [

      //
      // Drop test demo: drops a dynamic object onto a static object
      //
      state SimulationDropTest (
        playback   : simulation::Playback,
        simulation : Simulation = simulation::init_drop_test (render_context)
      )

      //
      // Capsule v. capsule collision
      //
      state SimulationCollideCapsuleCapsule (
        playback              : simulation::Playback,
        simulation_generation : (Simulation, u64) = {
          let (simulation, generation) =
            simulation::init_collide_capsule_capsule (render_context, rng);
          log::info!("simulation collide capsule capsule generation [{}]",
            generation);
          (simulation, generation)
        }
      )

      //
      // Stack test demo: stacks dynamic objects
      //
      state SimulationStackTest (
        playback   : simulation::Playback,
        simulation : Simulation = simulation::init_stack_test (render_context)
      )

      //
      // BallPit test demo: drop spheres into container
      //
      state SimulationBallPitTest (
        playback              : simulation::Playback,
        simulation_generation : (Simulation, u64) = {
          let (simulation, generation) =
            simulation::init_ball_pit_test (render_context, rng);
          log::info!("simulation collide capsule capsule generation [{}]",
            generation);
          (simulation, generation)
        }
      )

      //
      // Resolve positions of intersecting capsules with mass
      //
      state PenetrationCapsuleCapsule (
        // lines to show delta position of resolved capsules
        center_indices_lines : glium::IndexBuffer <u32> = {
          use render::resource::default::draw3d::MeshId;
          let capsule_range = render_context.resource.draw3d.instanced_meshes()
            [MeshId::Capsule as usize].instances_range.clone();
          glium::IndexBuffer::new (
            &render_context.glium_display,
            glium::index::PrimitiveType::LinesList,
            &[capsule_range.start as u32 + 0, capsule_range.start as u32 + 2,
              capsule_range.start as u32 + 1, capsule_range.start as u32 + 3]
          ).unwrap()
        }
      )

      //
      // Resolve positions of intersecting cuboids with mass
      //
      state PenetrationCuboidCuboid (
        // lines to show delta position of resolved shapes
        center_indices_lines : glium::IndexBuffer <u32> = {
          let cuboid_range  = render_context.resource.draw3d
            .instanced_aabb_lines().clone();
          glium::IndexBuffer::new (
            &render_context.glium_display,
            glium::index::PrimitiveType::LinesList,
            &[cuboid_range.start as u32 + 0, cuboid_range.start as u32 + 2,
              cuboid_range.start as u32 + 1, cuboid_range.start as u32 + 3]
          ).unwrap()
        }
      )

      //
      // Resolve positions of intersecting capsule and cuboid
      //
      state PenetrationCapsuleCuboid (
        // lines to show delta position of resolved shapes
        center_indices_lines : glium::IndexBuffer <u32> = {
          use render::resource::default::draw3d::MeshId;
          let capsule_range = render_context.resource.draw3d.instanced_meshes()
            [MeshId::Capsule as usize].instances_range.clone();
          let cuboid_range  = render_context.resource.draw3d
            .instanced_aabb_lines().clone();
          glium::IndexBuffer::new (
            &render_context.glium_display,
            glium::index::PrimitiveType::LinesList,
            &[capsule_range.start as u32 + 0, capsule_range.start as u32 + 1,
              cuboid_range.start as u32 + 0, cuboid_range.start as u32 + 1]
          ).unwrap()
        }
      )

    ]
////////////////////////////////////////////////////////////////////////////////
//  events                                                                    //
////////////////////////////////////////////////////////////////////////////////
    EVENTS [

      //
      // SimulationDropTest events
      //
      // Transition from any state to simulation drop test
      event ToSimulationDropTest <*> => <SimulationDropTest> ()
      // Set the single step flag
      event StepSimulationDropTest <SimulationDropTest> ()
        { playback } => { playback.single_step = true; }
      // Set the single step reverse flag
      event StepReverseSimulationDropTest <SimulationDropTest> ()
        { playback } => { playback.single_step_reverse = true; }
      // Pause simulation
      event PauseSimulationDropTest <SimulationDropTest> ()
        { playback } => { playback.paused = true; }
      // Unpause simulation
      event ResumeSimulationDropTest <SimulationDropTest> ()
        { playback } => { playback.paused = false; }
      // Simulation update
      event UpdateSimulationDropTest <SimulationDropTest> () {
        simulation, playback
      } => {
        simulation::update (render_context, playback, simulation)
      }
      // Restart simulation
      event RestartSimulationDropTest <SimulationDropTest> () {
        simulation
      } => { simulation::restart_drop_test (render_context, simulation) }

      //
      // SimulationCollideCapsuleCapsule events
      //
      // Transition from any state to simulation capsule v. capsule collision
      event ToSimulationCollideCapsuleCapsule
        <*> => <SimulationCollideCapsuleCapsule> ()
      // Set the single step flag
      event StepSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
        { playback } => { playback.single_step = true; }
      // Set the single step reverse flag
      event StepReverseSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
        { playback } => { playback.single_step_reverse = true; }
      // Pause simulation
      event PauseSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
        { playback } => { playback.paused = true; }
      // Unpause simulation
      event ResumeSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
        { playback } => { playback.paused = false; }
      // Simulation update
      event UpdateSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
      { simulation_generation, playback } => {
        simulation::update (render_context, playback,
          &mut simulation_generation.0)
      }
      // Generate new random capsule collision
      event NextSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
      { simulation_generation } => {
        let (ref mut simulation, ref mut generation_counter) =
          simulation_generation;
        *simulation = Simulation::new_collide_capsule_capsule (rng);
        *generation_counter += 1;
        log::info!("simulation collide capsule capsule generation [{}]",
          generation_counter);
      }
      // Restart simulation
      event RestartSimulationCollideCapsuleCapsule
        <SimulationCollideCapsuleCapsule> ()
      { simulation_generation } => {
        simulation::restart_collide_capsule_capsule (render_context,
          &mut simulation_generation.0)
      }

      //
      // SimulationStackTest events
      //
      // Transition from any state to simulation stack test
      event ToSimulationStackTest <*> => <SimulationStackTest> ()
      // Set the single step flag
      event StepSimulationStackTest <SimulationStackTest> ()
        { playback } => { playback.single_step = true; }
      // Set the single step reverse flag
      event StepReverseSimulationStackTest <SimulationStackTest> ()
        { playback } => { playback.single_step_reverse = true; }
      // Pause simulation
      event PauseSimulationStackTest <SimulationStackTest> ()
        { playback } => { playback.paused = true; }
      // Unpause simulation
      event ResumeSimulationStackTest <SimulationStackTest> ()
        { playback } => { playback.paused = false; }
      // Simulation update
      event UpdateSimulationStackTest <SimulationStackTest> () {
        simulation, playback
      } => {
        simulation::update (render_context, playback, simulation)
      }
      // Restart simulation
      event RestartSimulationStackTest <SimulationStackTest> () {
        simulation
      } => { simulation::restart_stack_test (render_context, simulation) }

      //
      // SimulationBallPitTest events
      //
      // Transition from any state to simulation ball_pit test
      event ToSimulationBallPitTest <*> => <SimulationBallPitTest> ()
      // Set the single step flag
      event StepSimulationBallPitTest <SimulationBallPitTest> ()
        { playback } => { playback.single_step = true; }
      // Set the single step reverse flag
      event StepReverseSimulationBallPitTest <SimulationBallPitTest> ()
        { playback } => { playback.single_step_reverse = true; }
      // Pause simulation
      event PauseSimulationBallPitTest <SimulationBallPitTest> ()
        { playback } => { playback.paused = true; }
      // Unpause simulation
      event ResumeSimulationBallPitTest <SimulationBallPitTest> ()
        { playback } => { playback.paused = false; }
      // Simulation update
      event UpdateSimulationBallPitTest <SimulationBallPitTest> () {
        simulation_generation, playback
      } => {
        simulation::update (render_context, playback,
          &mut simulation_generation.0)
      }
      // Restart simulation
      event RestartSimulationBallPitTest <SimulationBallPitTest> () {
        simulation_generation
      } => {
        simulation::restart_ball_pit_test (render_context,
          &mut simulation_generation.0)
      }
      // Generate new random ball pit test
      event NextSimulationBallPitTest
        <SimulationBallPitTest> ()
      { simulation_generation }  => {
        let (ref mut simulation, ref mut generation_counter) =
          simulation_generation;
        *simulation = Simulation::new_ball_pit_test (rng);
        *generation_counter += 1;
        log::info!("simulation ball pit test generation [{}]",
          generation_counter);
      }

      //
      // PenetrationCapsuleCapsule events
      //
      // Transition from any state to penetration resolve capsule/capsule state
      event ToPenetrationCapsuleCapsule <*> => <PenetrationCapsuleCapsule> ()
        {}  => { penetration::init_capsule_capsule (render_context, rng) }
      // Generate and resolve the next random capsule pair
      event NextPenetrationCapsuleCapsule <PenetrationCapsuleCapsule> ()
        {}  => { penetration::next_capsule_capsule (render_context, rng) }
      // Draw a penetration resolve capsule/capsule frame
      event DrawPenetrationCapsuleCapsule <PenetrationCapsuleCapsule>
        (glium_frame : &'event mut glium::Frame)
      { center_indices_lines } => {
        penetration::draw_capsule_capsule (
          render_context, glium_frame, center_indices_lines)
      }

      //
      // PositionPenetrationCuboidCuboid events
      //
      // Transition from any state to penetration resolve cuboid/cuboid state
      event ToPenetrationCuboidCuboid <*> => <PenetrationCuboidCuboid> ()
        {}  => { penetration::init_cuboid_cuboid (render_context, rng) }
      // Generate and resolve the next random cuboid pair
      event NextPenetrationCuboidCuboid <PenetrationCuboidCuboid> ()
        {}  => { penetration::next_cuboid_cuboid (render_context, rng) }
      // Draw a penetration resolve cuboid/cuboid frame
      event DrawPenetrationCuboidCuboid <PenetrationCuboidCuboid>
        (glium_frame : &'event mut glium::Frame)
      { center_indices_lines } => {
        penetration::draw_cuboid_cuboid (
          render_context, glium_frame, center_indices_lines)
      }

      //
      // PositionPenetrationCapsuleCuboid events
      //
      // Transition from any state to penetration resolve capsule/cuboid state
      event ToPenetrationCapsuleCuboid <*> => <PenetrationCapsuleCuboid> ()
        {}  => { penetration::init_capsule_cuboid (render_context, rng) }
      // Generate and resolve the next random capsule/abb pair
      event NextPenetrationCapsuleCuboid <PenetrationCapsuleCuboid> ()
        {}  => { penetration::next_capsule_cuboid (render_context, rng) }
      // Draw a penetration resolve capsule/cuboid frame
      event DrawPenetrationCapsuleCuboid <PenetrationCapsuleCuboid>
        (glium_frame : &'event mut glium::Frame)
      { center_indices_lines } => {
        penetration::draw_capsule_cuboid (
          render_context, glium_frame, center_indices_lines)
      }

    ]
////////////////////////////////////////////////////////////////////////////////
//  initial state                                                             //
////////////////////////////////////////////////////////////////////////////////
    initial_state: SimulationDropTest
    //initial_state: SimulationCollideCapsuleCapsule
  }
}

pub (crate) fn print_testbed_modes_prompt() {
  println!(">>> linear-sim testbed modes of operation:");
  println!("  Press '7' for ball pit test");
  println!("  Press '6' for stack test");
  println!("  Press '5' for capsule/capsule collision test");
  println!("  Press '4' for capsule/cuboid penetration resolution test");
  println!("  Press '3' for cuboid/cuboid penetration resolution test");
  println!("  Press '2' for capsule/capsule penetration resolution test");
  println!("  Press '1' to return to simulation drop test");
}

pub (crate) fn reset_render_context (
  render_context : &mut Render <render::resource::Default>
) {
  use crate::{INITIAL_CLEAR_COLOR, INITIAL_2D_ZOOM, INITIAL_3D_POSITION};
  render_context.reset();
  render_context.clear_color = INITIAL_CLEAR_COLOR;
  render_context.camera3d_position_set (INITIAL_3D_POSITION.into());
  render_context.camera2d_zoom_set (INITIAL_2D_ZOOM);
}
