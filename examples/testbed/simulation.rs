use {math, rand_xorshift};
use rand_xorshift::XorShiftRng;
use vec_map::VecMap;
use gl_utils as gl;
use gl_utils::*;
use linear_sim::*;

use crate::{component, program};

const INITIAL_HISTORY_LENGTH : usize    = 4096;

//
//  DropTest types and constants
//
mod drop_test {
  pub const INITIAL_CAMERA3D_POSITION : [f32; 3] = [-4.3178396, 2.0710685, 6.0];
    //[0.0, 0.5 * render::resource::MESH_3D_GRID_DIMS as f32, 2.0];
  pub const INITIAL_CAMERA3D_YAW      : f32 = 5.4977875;
  pub const INITIAL_CAMERA3D_PITCH    : f32 = -0.5235988;

  pub const GRAVITY_ACCELERATION : [f64; 3] = [0.0, 0.0, (-9.8)/ (60.0 * 60.0)];

  pub const BLOCK_POSITION       : [f64; 3] = [0.0, 1.0, 1.6];
  pub const BLOCK_HALF_EXTENTS   : [f64; 3] = [2.0, 3.0, 0.2];

  pub const GROUND_POSITION      : [f64; 3] = [0.0, 0.0, 0.0];

  //pub const SPHERE_POSITION      : [f64; 3] = [0.0, 1.0, 6.0];    // centered
  pub const SPHERE_POSITION      : [f64; 3] = [-2.3, 4.3, 6.0];   // corner
  pub const SPHERE_RADIUS        : f64      = 0.5;

  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectStatic {
    Block = 0,
    Ground
  }
  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectDynamic {
    BallA = 0,
    BallB,
    BallC
  }
}

//
//  StackTest types and constants
//
mod stack_test {
  pub const INITIAL_CAMERA3D_POSITION : [f32; 3] = [-8.0, 1.0, 8.0];
  pub const INITIAL_CAMERA3D_YAW      : f32 = -1.6;
  pub const INITIAL_CAMERA3D_PITCH    : f32 = -0.25;

  pub const GRAVITY_ACCELERATION : [f64; 3] = [0.0, 0.0, (-9.8)/ (60.0 * 60.0)];

  pub const GROUND_POSITION      : [f64; 3] = [0.0, 0.0, 0.0];

  pub const CUBOID_POSITION      : [f64; 3] = [0.0, 1.0, 6.0];    // centered
  pub const CUBOID_HALF_EXTENT   : f64      = 0.5;

  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectStatic {
    Ground = 0
  }
  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectDynamic {
    BoxA = 0,
    BoxB,
    BoxC
  }
}

//
//  BallPitTest types and constants
//
mod ball_pit_test {
  /// Skip ahead to start on this generation
  pub const GENERATION_START          : u64 = 0;

  pub const INITIAL_CAMERA3D_POSITION : [f32; 3] = [-8.0, 1.0, 8.0];
  pub const INITIAL_CAMERA3D_YAW      : f32      = -1.6;
  pub const INITIAL_CAMERA3D_PITCH    : f32      = -0.25;

  pub const GRAVITY_ACCELERATION      : [f64; 3] =
    [0.0, 0.0, (-9.8) / (60.0 * 60.0)];

  pub const PIT_DEPTH                 : f64      = 8.0;
  pub const BLOCK_THICKNESS           : f64      = 0.4;
  pub const BLOCK_BOTTOM_POSITION     : [f64; 3] = [ 0.0,  1.0, 1.6];
  pub const BLOCK_BOTTOM_HALF_EXTENTS : [f64; 3] = [
    2.0 + BLOCK_THICKNESS / 2.0,
    2.0 + BLOCK_THICKNESS / 2.0,
    BLOCK_THICKNESS / 2.0
  ];
  pub const BLOCK_LEFT_POSITION       : [f64; 3] = [
    BLOCK_BOTTOM_POSITION[0] - BLOCK_BOTTOM_HALF_EXTENTS[0] +
      BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_POSITION[1],
    BLOCK_BOTTOM_POSITION[2] + PIT_DEPTH / 2.0 + BLOCK_THICKNESS / 2.0
  ];
  pub const BLOCK_LEFT_HALF_EXTENTS   : [f64; 3] = [
    BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_HALF_EXTENTS[1] - BLOCK_THICKNESS,
    PIT_DEPTH / 2.0
  ];
  pub const BLOCK_RIGHT_POSITION      : [f64; 3] = [
    BLOCK_BOTTOM_POSITION[0] + BLOCK_BOTTOM_HALF_EXTENTS[0] -
      BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_POSITION[1],
    BLOCK_BOTTOM_POSITION[2] + PIT_DEPTH / 2.0 + BLOCK_THICKNESS / 2.0
  ];
  pub const BLOCK_RIGHT_HALF_EXTENTS  : [f64; 3] = [
    BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_HALF_EXTENTS[1] - BLOCK_THICKNESS,
    PIT_DEPTH / 2.0
  ];
  pub const BLOCK_FRONT_POSITION      : [f64; 3] = [
    BLOCK_BOTTOM_POSITION[0],
    BLOCK_BOTTOM_POSITION[1] - BLOCK_BOTTOM_HALF_EXTENTS[1] +
      BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_POSITION[2] + PIT_DEPTH / 2.0 + BLOCK_THICKNESS / 2.0
  ];
  pub const BLOCK_FRONT_HALF_EXTENTS  : [f64; 3] = [
    BLOCK_BOTTOM_HALF_EXTENTS[0],
    BLOCK_THICKNESS / 2.0,
    PIT_DEPTH / 2.0
  ];
  pub const BLOCK_BACK_POSITION       : [f64; 3] = [
    BLOCK_BOTTOM_POSITION[0],
    BLOCK_BOTTOM_POSITION[1] + BLOCK_BOTTOM_HALF_EXTENTS[1] -
      BLOCK_THICKNESS / 2.0,
    BLOCK_BOTTOM_POSITION[2] + PIT_DEPTH / 2.0 + BLOCK_THICKNESS / 2.0
  ];
  pub const BLOCK_BACK_HALF_EXTENTS   : [f64; 3] = [
    BLOCK_BOTTOM_HALF_EXTENTS[0],
    BLOCK_THICKNESS / 2.0,
    PIT_DEPTH / 2.0
  ];

  pub const N_BALLS : u32 = 40;

  pub const SPHERE_RADIUS   : f64      = 0.5;
  pub const SPHERE_POSITION : [f64; 3] = [
    BLOCK_BOTTOM_POSITION[0],
    BLOCK_BOTTOM_POSITION[1],
    BLOCK_BOTTOM_POSITION[2] + 12.0
  ];
  pub const SPHERE_RANGE : [f64; 3] = [
    BLOCK_BOTTOM_HALF_EXTENTS[0] - BLOCK_THICKNESS - SPHERE_RADIUS - 0.05,
    BLOCK_BOTTOM_HALF_EXTENTS[1] - BLOCK_THICKNESS - SPHERE_RADIUS - 0.05,
    6.0
  ];

  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectStatic {
    Bottom = 0,
    Left,
    Right,
    Front,
    Back
  }
}


//
//  CollideCapsuleCapsule types and constants
//
mod collide_capsule_capsule {
  use gl_utils::render::resource::default;

  /// Number of capsules to generate in each example.
  // TODO: setting this to a number not too much larger than ~5 can run into
  // deadlock situations without contact handling implemented
  pub const CAPSULE_COUNT           : usize = 5;
  /// Skip ahead to start on this generation
  pub const GENERATION_START        : u64 = 14;

  pub const DENSITY_MIN             : f64 =   150.0;   // balsa wood: 160 kg/m^3
  pub const DENSITY_MAX             : f64 = 20000.0;   // gold: 19,320 kg/m^3

  pub const CAPSULE_RADIUS_MIN      : f64 =     0.05;  //  5cm
  pub const CAPSULE_RADIUS_MAX      : f64 =     0.7;   // 70cmm
  pub const CAPSULE_HALF_HEIGHT_MIN : f64 =     0.0;   //  0cm
  pub const CAPSULE_HALF_HEIGHT_MAX : f64 =     2.0;   //  2m

  pub const SPEED_MIN               : f64 = 1.0/120.0; // 2m/s   @ 60hz
  pub const SPEED_MAX               : f64 = 2.5;       // 150m/s @ 60hz

  pub const TIME_TARGET             : f64 =  1.0;      // 1s
  /// Note this does not control the simulation speed but is based on a
  /// hard-coded vsync of 60hz; a higher or lower refresh rate will run faster
  /// or slower accordingly
  pub const STEPS_PER_SECOND        : f64 = 60.0;      // 60hz

  pub const INITIAL_CAMERA3D_POSITION : [f32; 3] =
    [0.0, -(1.0 * default::draw3d::MESH_GRID_DIMS as f32), 2.0];

  #[allow(dead_code)]
  #[derive(Clone, Copy, Debug, Eq, PartialEq)]
  pub enum ObjectDynamic {
    CapsuleA = 0,
    CapsuleB,
    CapsuleC
  }
}

////////////////////////////////////////////////////////////////////////////////
//  structs                                                                   //
////////////////////////////////////////////////////////////////////////////////

/// System state and history
#[derive(Debug)]
pub struct Simulation {
  pub system  : System <integrator::SemiImplicitEuler>,
  pub history : Vec <System <integrator::SemiImplicitEuler>>
}

/// Simulation playback control state
#[derive(Debug,Default)]
pub struct Playback {
  pub paused              : bool,
  pub single_step         : bool,
  pub single_step_reverse : bool
}

////////////////////////////////////////////////////////////////////////////////
//  public functions                                                          //
////////////////////////////////////////////////////////////////////////////////

pub fn init_drop_test (render_context : &mut Render <render::resource::Default>)
  -> Simulation
{
  use self::drop_test::*;
  use render::resource::default::draw3d;
  use math::num_traits::Zero;
  //
  // reset and initialize render context per-instance vertex data
  //
  program::reset_render_context (render_context);
  // position camera manually
  render_context.camera3d_position_set (INITIAL_CAMERA3D_POSITION.into());
  render_context.camera3d_rotate (
    math::Rad (INITIAL_CAMERA3D_YAW), math::Rad (INITIAL_CAMERA3D_PITCH),
    math::Rad::zero());

  //
  // create new simulation state and vertex data
  //
  let simulation = Simulation::new_drop_test();
  let grid_vertex_data = render::resource::Default::debug_grid_vertices();
  let (capsule_vertex_data, aabb_vertex_data, sphere_vertex_data) =
    simulation.object_vertex_data();
  let aabb_lines = Some (&aabb_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Sphere as usize, &sphere_vertex_data[..])
      .is_none());
    v
  };
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  //
  // welcome
  //
  program::print_testbed_modes_prompt();
  println!("linear-sim simulation drop test");
  println!("  press 'P' to pause/resume");
  println!("  press '.' to step forward");
  println!("  press ',' to step backward");
  println!("  press 'Backspace' to reset");

  simulation
}

/// Returns the initial simulation and generation number
pub fn init_collide_capsule_capsule (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng,
) -> (Simulation, u64) {
  use rand::SeedableRng;
  use self::collide_capsule_capsule::*;
  use render::resource::default::draw3d;

  //
  // reset render context and rng
  //
  program::reset_render_context (render_context);
  render_context.camera3d_position_set (INITIAL_CAMERA3D_POSITION.into());
  *rng = XorShiftRng::seed_from_u64 (0);
  //
  // create new simulation state and vertex data
  //
  let simulation = {
    let mut generation = 0;
    loop {
      let simulation = Simulation::new_collide_capsule_capsule (rng);
      if generation == GENERATION_START { break simulation }
      generation += 1;
    }
  };
  let grid_vertex_data = render::resource::Default::debug_grid_vertices();
  let (capsule_vertex_data, aabb_vertex_data, sphere_vertex_data) =
    simulation.object_vertex_data();
  let aabb_lines = Some (&aabb_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Sphere  as usize, &sphere_vertex_data[..])
      .is_none());
    v
  };
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  //
  // welcome
  //
  program::print_testbed_modes_prompt();
  println!("linear-sim simulation collide capsule capsule");
  println!("  press 'P' to pause/resume");
  println!("  press '.' to step forward");
  println!("  press ',' to step backward");
  println!("  press 'Backspace' to reset current pair");
  println!("  press 'Tab' to generate a new collision pair");

  (simulation, GENERATION_START)
}

pub fn init_stack_test (render_context : &mut Render <render::resource::Default>)
  -> Simulation
{
  use self::stack_test::*;
  use render::resource::default::draw3d;
  use math::num_traits::Zero;
  //
  // reset and initialize render context per-instance vertex data
  //
  program::reset_render_context (render_context);
  // position camera manually
  render_context.camera3d_position_set (INITIAL_CAMERA3D_POSITION.into());
  render_context.camera3d_rotate (
    math::Rad (INITIAL_CAMERA3D_YAW), math::Rad (INITIAL_CAMERA3D_PITCH),
    math::Rad::zero());

  //
  // create new simulation state and vertex data
  //
  let simulation = Simulation::new_stack_test();
  let grid_vertex_data = render::resource::Default::debug_grid_vertices();
  let (capsule_vertex_data, aabb_vertex_data, sphere_vertex_data) =
    simulation.object_vertex_data();
  let aabb_lines = Some (&aabb_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Sphere as usize,  &sphere_vertex_data[..])
      .is_none());
    v
  };
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  //
  // welcome
  //
  program::print_testbed_modes_prompt();
  println!("linear-sim simulation stack test");
  println!("  press 'P' to pause/resume");
  println!("  press '.' to step forward");
  println!("  press ',' to step backward");
  println!("  press 'Backspace' to reset");

  simulation
}

/// Returns the initial simulation and generation number
pub fn init_ball_pit_test (
  render_context : &mut Render <render::resource::Default>,
  rng            : &mut rand_xorshift::XorShiftRng
) -> (Simulation, u64) {
  use rand::SeedableRng;
  use math::num_traits::Zero;
  use render::resource::default::draw3d;
  use self::ball_pit_test::*;
  //
  // reset render context and rng
  //
  program::reset_render_context (render_context);
  // position camera manually
  render_context.camera3d_position_set (INITIAL_CAMERA3D_POSITION.into());
  render_context.camera3d_rotate (
    math::Rad (INITIAL_CAMERA3D_YAW), math::Rad (INITIAL_CAMERA3D_PITCH),
    math::Rad::zero());
  // reset rng
  *rng = XorShiftRng::seed_from_u64 (1);

  //
  // create new simulation state and vertex data
  //
  let simulation = {
    let mut generation = 0;
    loop {
      let simulation = Simulation::new_ball_pit_test (rng);
      if generation == GENERATION_START { break simulation }
      generation += 1;
    }
  };
  let grid_vertex_data = render::resource::Default::debug_grid_vertices();
  let (capsule_vertex_data, aabb_vertex_data, sphere_vertex_data) =
    simulation.object_vertex_data();
  let aabb_lines = Some (&aabb_vertex_data[..]);
  let meshes = {
    let mut v = VecMap::with_capacity (2);
    assert!(v.insert (draw3d::MeshId::Grid    as usize, &grid_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Capsule as usize, &capsule_vertex_data[..])
      .is_none());
    assert!(v.insert (draw3d::MeshId::Sphere as usize,  &sphere_vertex_data[..])
      .is_none());
    v
  };
  render_context.resource.draw3d.instance_vertices_set (
    &render_context.glium_display,
    draw3d::InstancesInit {
      billboards: VecMap::default(),
      aabb_lines,
      meshes,
      .. Default::default()
    }
  );

  //
  // welcome
  //
  program::print_testbed_modes_prompt();
  println!("linear-sim simulation ball pit test");
  println!("  press 'P' to pause/resume");
  println!("  press '.' to step forward");
  println!("  press ',' to step backward");
  println!("  press 'Backspace' to reset");

  (simulation, GENERATION_START)
}

pub fn update (
  render_context : &mut Render <render::resource::Default>,
  playback       : &mut Playback,
  simulation     : &mut Simulation
) {
  if !playback.paused {
    step (render_context, simulation);
  } else if playback.single_step {
    step (render_context, simulation);
    playback.single_step = false;
  } else if playback.single_step_reverse {
    step_reverse (render_context, simulation);
    playback.single_step_reverse = false;
  }
}

pub fn restart_drop_test (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  *simulation = Simulation::new_drop_test();
  update_render_context (render_context, &mut simulation.system);
}

pub fn restart_stack_test (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  *simulation = Simulation::new_stack_test();
  update_render_context (render_context, &mut simulation.system);
}

pub fn restart_ball_pit_test (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  simulation.system = simulation.history[0].clone();
  simulation.history.truncate (1);
  update_render_context (render_context, &mut simulation.system);
}

pub fn restart_collide_capsule_capsule (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  simulation.system = simulation.history[0].clone();
  simulation.history.truncate (1);
  update_render_context (render_context, &mut simulation.system);
}

////////////////////////////////////////////////////////////////////////////////
//  impls                                                                     //
////////////////////////////////////////////////////////////////////////////////

impl Simulation {
  pub fn new_drop_test() -> Self {
    use geometry::shape;
    use self::drop_test::*;

    let mut system  = System::<integrator::SemiImplicitEuler>::default();
    let mut history = Vec::with_capacity (INITIAL_HISTORY_LENGTH);

    // create static object: axis-aligned block (cuboid)
    let myblock = {
      let position   = component::Position (BLOCK_POSITION.into());
      let bound      = component::Bound    (shape::Bounded::from (
        shape::Cuboid::noisy (BLOCK_HALF_EXTENTS.into())).into());
      let material   = component::MATERIAL_STONE;
      let collidable = true;
      object::Static { position, bound, material, collidable }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      myblock, Some (object::Key::from (ObjectStatic::Block as u32))
    ));
    assert_create_object_success (result);

    // create static object: ground orthant
    let myground = {
      let position   = component::Position (GROUND_POSITION.into());
      let bound      = component::Bound    (shape::Unbounded::from (
        shape::Orthant::from (math::SignedAxis3::PosZ)).into());
      let material   = component::MATERIAL_STONE;
      let collidable = true;
      object::Static { position, bound, material, collidable }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      myground, Some (object::Key::from (ObjectStatic::Ground as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: sphere A
    let mysphere = {
      let position    = component::Position (SPHERE_POSITION.into());
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (SPHERE_RADIUS)).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mysphere, Some (object::Key::from (ObjectDynamic::BallA as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: sphere B
    let mysphere = {
      let position    = component::Position (
        math::Point3::from (SPHERE_POSITION) +
        math::Vector3::new (-0.01, -0.015, 4.0)
      );
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (SPHERE_RADIUS)).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mysphere, Some (object::Key::from (ObjectDynamic::BallB as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: sphere C
    let mysphere = {
      let position    = component::Position (
        math::Point3::from (SPHERE_POSITION) +
        math::Vector3::new (0.02, 0.03, 2.0)
      );
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (SPHERE_RADIUS)).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mysphere, Some (object::Key::from (ObjectDynamic::BallC as u32))
    ));
    assert_create_object_success (result);

    // create gravity force
    let gravity = force::Gravity { acceleration: GRAVITY_ACCELERATION.into() };
    assert!(system.handle_event (event::Input::SetGravity (gravity)).is_empty());

    history.push (system.clone());

    Simulation { system, history }
  }

  pub fn new_stack_test() -> Self {
    use geometry::shape;
    use self::stack_test::*;

    let mut system  = System::<integrator::SemiImplicitEuler>::default();
    let mut history = Vec::with_capacity (INITIAL_HISTORY_LENGTH);

    // create static object: ground orthant
    let myground = {
      let position   = component::Position (GROUND_POSITION.into());
      let bound      = component::Bound    (shape::Unbounded::from (
        shape::Orthant::from (math::SignedAxis3::PosZ)).into());
      let material   = component::MATERIAL_STONE;
      let collidable = true;
      object::Static { position, bound, material, collidable }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      myground, Some (object::Key::from (ObjectStatic::Ground as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: cuboid A
    let mycuboid = {
      let position    = component::Position (CUBOID_POSITION.into());
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([
          CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT
        ])).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mycuboid, Some (object::Key::from (ObjectDynamic::BoxA as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: cuboid B
    let mycuboid = {
      let position    = component::Position (
        math::Point3::from (CUBOID_POSITION) +
        math::Vector3::new (-0.01, -0.015, 4.0)
      );
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([
          CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT
        ])).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mycuboid, Some (object::Key::from (ObjectDynamic::BoxB as u32))
    ));
    assert_create_object_success (result);

    // create dynamic object: cuboid C
    let mycuboid = {
      let position    = component::Position (
        math::Point3::from (CUBOID_POSITION) +
        math::Vector3::new (0.02, 0.03, 2.0)
      );
      let mass        = component::Mass::new (20.0);
      let derivatives = component::Derivatives::zero();
      let drag        = component::Drag::zero();
      let bound       = component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([
          CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT, CUBOID_HALF_EXTENT
        ])).into());
      let material    = component::MATERIAL_STONE;
      let collidable  = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }.into()
    };
    let result = system.handle_event (event::Input::CreateObject (
      mycuboid, Some (object::Key::from (ObjectDynamic::BoxC as u32))
    ));
    assert_create_object_success (result);

    // create gravity force
    let gravity = force::Gravity { acceleration: GRAVITY_ACCELERATION.into() };
    assert!(system.handle_event (event::Input::SetGravity (gravity)).is_empty());

    history.push (system.clone());

    Simulation { system, history }
  }

  pub fn new_ball_pit_test (rng : &mut rand_xorshift::XorShiftRng) -> Self {
    use geometry::shape;
    use self::ball_pit_test::*;

    let mut system  = System::<integrator::SemiImplicitEuler>::default();
    let mut history = Vec::with_capacity (INITIAL_HISTORY_LENGTH);

    let mut make_block = |object_index, position, half_extents| {
      let block = {
        let position   = component::Position (position);
        let bound      = component::Bound (
          shape::Bounded::from (shape::Cuboid::noisy (half_extents)).into()
        );
        let material   = component::MATERIAL_STONE;
        let collidable = true;
        object::Static { position, bound, material, collidable }.into()
      };
      let result = system.handle_event (event::Input::CreateObject (
        block, Some (object::Key::from (object_index))
      ));
      assert_create_object_success (result);
    };
    // create static blocks
    make_block (ObjectStatic::Bottom as u32, BLOCK_BOTTOM_POSITION.into(),
      BLOCK_BOTTOM_HALF_EXTENTS.into());
    make_block (ObjectStatic::Left as u32,   BLOCK_LEFT_POSITION.into(),
      BLOCK_LEFT_HALF_EXTENTS.into());
    make_block (ObjectStatic::Right as u32,  BLOCK_RIGHT_POSITION.into(),
      BLOCK_RIGHT_HALF_EXTENTS.into());
    make_block (ObjectStatic::Front as u32,  BLOCK_FRONT_POSITION.into(),
      BLOCK_FRONT_HALF_EXTENTS.into());
    make_block (ObjectStatic::Back as u32,   BLOCK_BACK_POSITION.into(),
      BLOCK_BACK_HALF_EXTENTS.into());

    let mut generate_random_ball = |object_index| {
      use rand::Rng;
      let mut position    = [
        SPHERE_POSITION[0] +
          rng.gen_range (-SPHERE_RANGE[0]..SPHERE_RANGE[0]),
        SPHERE_POSITION[1] +
          rng.gen_range (-SPHERE_RANGE[1]..SPHERE_RANGE[1]),
        SPHERE_POSITION[2] +
          rng.gen_range (-SPHERE_RANGE[2]..SPHERE_RANGE[2])
      ].into();
      loop {
        let sphere = {
          let position    = component::Position (position);
          let mass        = component::Mass::new (20.0);
          let derivatives = component::Derivatives::zero();
          let drag        = component::Drag::zero();
          let bound       = component::Bound (shape::Bounded::from (
            shape::Sphere::noisy (SPHERE_RADIUS)).into());
          let material    = component::MATERIAL_STONE;
          let collidable  = true;
          object::Dynamic {
            position, mass, derivatives, drag, bound, material, collidable
          }.into()
        };
        let mut result = system.handle_event (event::Input::CreateObject (
          sphere, Some (object::Key::from (object_index))
        ));
        match result.pop().unwrap() {
          event::Output::CreateObjectResult (
            event::CreateObjectResult::Created (_)
          ) => {
            debug_assert!(result.is_empty());
            break
          }
          _ => {}
        }
        position.0.z += 2.0;
      }
    };

    for i in 0..N_BALLS {
      generate_random_ball (i);
    }

    // create gravity force
    let gravity = force::Gravity { acceleration: GRAVITY_ACCELERATION.into() };
    assert!(system.handle_event (event::Input::SetGravity (gravity)).is_empty());

    history.push (system.clone());

    Simulation { system, history }
  }

  pub fn new_collide_capsule_capsule (rng : &mut rand_xorshift::XorShiftRng)
    -> Self
  {
    use self::collide_capsule_capsule::*;

    fn generate_random_capsule (rng : &mut rand_xorshift::XorShiftRng)
      -> object::Dynamic
    {
      use std::f64::consts::*;
      use rand::Rng;
      use geometry::shape;
      use geometry::shape::Stereometric;

      const UNIT_Y : math::Vector3 <f64> =
        math::Vector3 { x: 0.0, y: 1.0, z: 0.0};
      let radius         = rng.gen_range (CAPSULE_RADIUS_MIN..CAPSULE_RADIUS_MAX);
      let half_height    = rng.gen_range (
        CAPSULE_HALF_HEIGHT_MIN..CAPSULE_HALF_HEIGHT_MAX);
      let density        = rng.gen_range (DENSITY_MIN..DENSITY_MAX);
      let shape          = shape::Capsule::noisy (radius, half_height);
      let volume         = *shape.volume();
      let mass           = component::Mass::new (density * volume);
      let speed          = rng.gen_range (SPEED_MIN..SPEED_MAX);
      let random_yaw     = math::Rad (rng.gen_range (0.0..2.0 * PI));
      let rotation_yaw   = math::Rotation3::from_angle_z (random_yaw);
      let random_pitch   = math::Rad (rng.gen_range (-0.5 * PI..0.5 * PI));
      let rotation_pitch = math::Rotation3::from_angle_x (random_pitch);
      let direction      = *(rotation_pitch * rotation_yaw) * UNIT_Y;
      let velocity       = speed * direction;
      let position       = component::Position (
        math::Point3 (-TIME_TARGET * STEPS_PER_SECOND * velocity));
      let derivatives    = component::Derivatives {
        velocity, .. component::Derivatives::zero()
      };
      let drag           = component::Drag (0.01);
      let bound          = component::Bound (shape::Bounded::from (shape).into());
      let material       = component::MATERIAL_STONE;
      let collidable     = true;
      object::Dynamic {
        position, mass, derivatives, drag, bound, material, collidable
      }
    }

    let mut system  = System::<integrator::SemiImplicitEuler>::default();
    let mut history = Vec::with_capacity (INITIAL_HISTORY_LENGTH);

    for i in 0..CAPSULE_COUNT {
      loop {
        let capsule    = generate_random_capsule (rng);
        let mut failed = false;
        for output_event in system.handle_event (event::Input::CreateObject (
          capsule.clone().into(), Some (object::Key::from (i as u32))
        )) {
          match output_event {
            event::Output::CreateObjectResult (
              event::CreateObjectResult::Intersection (..)
            ) => {
              failed = true;
              break
            }
            event::Output::CreateObjectResult (
              event::CreateObjectResult::Created (..)
            ) => {}
            _ => unreachable!("only create object result events should be returned")
          }
        }
        if !failed { break }
      }
    }

    history.push (system.clone());

    Simulation { system, history }
  }

  pub fn object_vertex_data (&self) -> (
    Vec <vertex::Vert3dOrientationScaleColor>,  // capsule vertex data
    Vec <vertex::Vert3dOrientationScaleColor>,  // cuboid (aabb) vertex data
    Vec <vertex::Vert3dOrientationScaleColor>   // sphere vertex data
  ) {
    use geometry::shape;

    // capsule vertex data
    let capsule_vertex_data = {
      let mut static_capsules : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_static().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Capsule (_))
            ) => Some (capsule_to_vertex (object, color::DEBUG_YELLOW)),
            _ => None   // skip
          }
        ).collect();
      let mut dynamic_capsules : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_dynamic().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Capsule (_))
            ) => Some (capsule_to_vertex (object, color::DEBUG_GOLD)),
            _ => None   // skip
          }
        ).collect();
      static_capsules.append (&mut dynamic_capsules);
      static_capsules
    };

    // aabb vertex data
    let aabb_vertex_data = {
      let mut static_aabbs : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_static().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Cuboid (_))
            ) => Some (cuboid_to_vertex (object, color::DEBUG_PINK)),
            _ => None   // skip
          }
        ).collect();
      let mut dynamic_aabbs : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_dynamic().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Cuboid (_))
            ) => Some (cuboid_to_vertex (object, color::DEBUG_RED)),
            _ => None   // skip
          }
        ).collect();
      static_aabbs.append (&mut dynamic_aabbs);
      static_aabbs
    };

    // sphere vertex data
    let sphere_vertex_data = {
      let mut static_spheres : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_static().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Sphere (_))
            ) => Some (sphere_to_vertex (object, color::DEBUG_YELLOW)),
            _ => None   // skip
          }
        ).collect();
      let mut dynamic_spheres : Vec <vertex::Vert3dOrientationScaleColor> =
        self.system.objects_dynamic().iter().filter_map (
          |(_, object)| match &object.bound {
            component::Bound (
              shape::Variant::Bounded (shape::Bounded::Sphere (_))
            ) => Some (sphere_to_vertex (object, color::DEBUG_GOLD)),
            _ => None   // skip
          }
        ).collect();
      static_spheres.append (&mut dynamic_spheres);
      static_spheres
    };

    (capsule_vertex_data, aabb_vertex_data, sphere_vertex_data)
  }
}

////////////////////////////////////////////////////////////////////////////////
//  private functions                                                         //
////////////////////////////////////////////////////////////////////////////////

/// Step forward and update render context
fn step (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  let Simulation { ref mut system, ref mut history } = simulation;
  let output_events = system.handle_event (event::Input::Step);

  log::info!("step [{}]", (system.step()-1).to_string());
  log::debug!("output events: {:?}", output_events);

  history.push (system.clone());
  update_render_context (render_context, system);
}

/// Step reverse and update render context
fn step_reverse (
  render_context : &mut Render <render::resource::Default>,
  simulation     : &mut Simulation
) {
  let Simulation { ref mut system, ref mut history } = simulation;
  if history.len() > 1 {
    history.pop();
    *system = history.last().unwrap().clone();
  }
  update_render_context (render_context, system);
}

/// Update render context
fn update_render_context (
  render_context : &mut Render <render::resource::Default>,
  system         : &mut System <integrator::SemiImplicitEuler>
) {
  use render::resource::default::draw3d::MeshId;
  let contacts = system.contacts();
  let mut dynamic_ids = vec![];
  for (id_a, id_b, _) in contacts.into_iter().flatten() {
    if id_a.kind == object::Kind::Dynamic {
      dynamic_ids.push (id_a);
    }
    debug_assert_eq!(id_b.kind, object::Kind::Dynamic);
    dynamic_ids.push (id_b);
  }
  let dynamic_object_color = |id| if dynamic_ids.contains (&id) {
    color::CYAN
  } else {
    color::DEBUG_GOLD
  };
  // capsules
  let capsule_vertex_data : Vec <vertex::Vert3dOrientationScaleColor> =
    system.objects_dynamic().iter().filter_map (|(i, object)|{
      use geometry::shape;
      let id    = object::Id { kind: object::Kind::Dynamic, key: i.into() };
      let color = dynamic_object_color (id);
      match &object.bound {
        component::Bound (shape::Variant::Bounded (shape::Bounded::Capsule (_)))
          => Some (capsule_to_vertex (object, color)),
        _ => None   // skip
      }
    }).collect();

  // cuboids
  let aabb_vertex_data : Vec <vertex::Vert3dOrientationScaleColor> =
    system.objects_dynamic().iter().filter_map (|(i, object)|{
      use geometry::shape;
      let id    = object::Id { kind: object::Kind::Dynamic, key: i.into() };
      let color = dynamic_object_color (id);
      match &object.bound {
        component::Bound (shape::Variant::Bounded (shape::Bounded::Cuboid (_)))
          => Some (cuboid_to_vertex (object, color)),
        _ => None   // skip
      }
    }).collect();

  // spheres
  let sphere_vertex_data : Vec <vertex::Vert3dOrientationScaleColor> =
    system.objects_dynamic().iter().filter_map (|(i, object)|{
      use geometry::shape;
      let id    = object::Id { kind: object::Kind::Dynamic, key: i.into() };
      let color = dynamic_object_color (id);
      match &object.bound {
        component::Bound (shape::Variant::Bounded (shape::Bounded::Sphere (_)))
          => Some (sphere_to_vertex (object, color)),
        _ => None   // skip
      }
    }).collect();

  if !capsule_vertex_data.is_empty() {
    render_context.resource.draw3d.instance_vertices_mesh_write (
      MeshId::Capsule as usize, capsule_vertex_data.as_slice());
  }
  if !aabb_vertex_data.is_empty() {
    render_context.resource.draw3d
      .instance_vertices_aabb_lines_write (aabb_vertex_data.as_slice());
  }
  if !sphere_vertex_data.is_empty() {
    render_context.resource.draw3d.instance_vertices_mesh_write (
      MeshId::Sphere as usize, sphere_vertex_data.as_slice());
  }
}

fn capsule_to_vertex <O : object::Bounded> (object : &O, color : [u8; 4])
  -> vertex::Vert3dOrientationScaleColor
{
  use geometry::shape;
  let capsule = {
    match object.bound() {
      component::Bound (shape::Variant::Bounded (
        shape::Bounded::Capsule (ref capsule)
      )) => capsule.capsule3 (object.position().0).numcast().unwrap(),
      _  => panic!("capsule to vertex called on a non-capsule object: {:?}", object)
    }
  };
  vertex::Vert3dOrientationScaleColor {
    color: color::rgba_u8_to_rgba_f32 (color),
    .. gl::util::capsule_to_vertex (capsule)
  }
}

fn cuboid_to_vertex <O : object::Bounded> (object : &O, color : [u8; 4])
  -> vertex::Vert3dOrientationScaleColor
{
  use geometry::shape;
  let aabb = {
    match object.bound() {
      component::Bound (shape::Variant::Bounded (
        shape::Bounded::Cuboid (ref cuboid)
      )) => cuboid.aabb3 (object.position().0).numcast().unwrap(),
      _  => panic!("cuboid to vertex called on a non-cuboid object: {:?}", object)
    }
  };
  vertex::Vert3dOrientationScaleColor {
    color: color::rgba_u8_to_rgba_f32 (color),
    .. gl::util::aabb_to_vertex (aabb)
  }
}

fn sphere_to_vertex <O : object::Bounded> (object : &O, color : [u8; 4])
  -> vertex::Vert3dOrientationScaleColor
{
  use geometry::shape;
  let sphere = {
    match object.bound() {
      component::Bound (shape::Variant::Bounded (
        shape::Bounded::Sphere (ref sphere)
      )) => sphere.sphere3 (object.position().0).numcast().unwrap(),
      _  => panic!("capsule to vertex called on a non-capsule object: {:?}", object)
    }
  };
  vertex::Vert3dOrientationScaleColor {
    color: color::rgba_u8_to_rgba_f32 (color),
    .. gl::util::sphere_to_vertex (sphere)
  }
}

fn assert_create_object_success (mut result : Vec <event::Output>) {
  match result.pop().unwrap() {
    event::Output::CreateObjectResult (event::CreateObjectResult::Created (_))
      => {}
    _ => unreachable!()
  }
  assert!(result.pop().is_none());
}
