use std::convert::TryFrom;

use crate::collision::{proximity, Proximity};

/// Describes the inter-penetration of a pair of bounded objects
#[derive(Clone, Debug, PartialEq)]
pub struct Intersection {
  penetration : Proximity,
  // TODO: include relation information
  //relation    : Relation
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Relation {
  /// Partial intersection
  Partial,
  AinB,
  BinA
}

impl Intersection {
  #[inline]
  pub fn penetration (&self) -> &Proximity {
    &self.penetration
  }
}
impl TryFrom <Proximity> for Intersection {
  type Error = Proximity;
  fn try_from (proximity : Proximity) -> Result <Self, Proximity> {
    match proximity.relation() {
      proximity::Relation::Intersect
        => Ok (Intersection { penetration: proximity }),
      _ => Err(proximity)
    }
  }
}
