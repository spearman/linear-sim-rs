use geometry::shape;

use crate::{collision, component, constraint, geometry, math, object};

/// Relation configuration of a pair of bounded objects.
///
/// - 'Disjoint' corresponds to a positive distance `> CONTACT_DISTANCE`
/// - 'Contact' corresponds to a positive distance `< CONTACT_DISTANCE`
/// - 'Inersect' corresponds to a strictly negative contact distance `< 0.0`
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Relation {
  Disjoint,
  Contact,
  Intersect
}

/// Result of a proximity query between a pair of bounded objects.
///
/// Note that the *axis vector* points from object A to object B, while the
/// *normal vector* points from object B to object A, but they should be
/// parallel when the axis vector is non-zero.
#[derive(Clone, Debug, PartialEq)]
pub struct Proximity {
  /// Signed distance.
  ///
  /// A positive distance indicates the separating distance for a disjoint
  /// result, and a negative distance indicates a penetration depth of an
  /// intersection result.
  pub distance  : f64,
  /// Non-normalized vector pointing along the axis of separation or penetration
  /// from object A to object B.
  ///
  /// For a disjoint configuration, adding this vector to the midpoint gives the
  /// nearest point on object B and subtracting this vector from the midpoint
  /// gives the nearest point on object A. Translating object A by this vector
  /// and translating objet B by its inverse will bring the objects into
  /// contact.
  ///
  /// For an intersecting configuration, translating object A by this vector and
  /// translating object B by its inverse will resolve the inter-penetration.
  ///
  /// Note this vector can be zero if the objects are in contact.
  pub half_axis : math::Vector3 <f64>,
  /// Indicates the midpoint of the proximity query
  pub midpoint  : math::Point3  <f64>,
  /// The unit normal to the separating plane directed from object B to object
  /// A
  pub normal    : math::Unit3 <f64>
}

impl Proximity {
  /// Proximity query.
  ///
  /// # Examples
  ///
  /// **Capsule v. capsule**
  ///
  /// By convention if the axes of capsules A and B intersect, the +X axis will
  /// be chosen for the axis vector and normal:
  ///
  /// ```
  /// # extern crate linear_sim;
  /// # use linear_sim::*;
  /// # use collision::*;
  /// # use geometry::shape;
  /// # use math;
  /// # fn main() {
  /// let a = object::Static {
  ///   position:   component::Position ([0.0, 0.0, 1.0].into()),
  ///   bound:      component::Bound (shape::Bounded::from (
  ///     shape::Capsule::noisy (2.0, 3.0)).into()),
  ///   material:   component::MATERIAL_STONE,
  ///   collidable: true
  /// };
  /// let b = object::Static {
  ///   position:   component::Position ([0.0, 0.0, -1.0].into()),
  ///   bound:      component::Bound (shape::Bounded::from (
  ///     shape::Capsule::noisy (1.0, 2.0)).into()),
  ///   material:   component::MATERIAL_STONE,
  ///   collidable: true
  /// };
  /// assert_eq!(
  ///   Proximity::query (&a, &b),
  ///   Proximity {
  ///     distance:  -3.0,
  ///     half_axis: [ 1.5, 0.0,  0.0].into(),
  ///     normal:    math::Unit3::axis_x(),
  ///     midpoint:  [-0.5, 0.0, -0.5].into()
  ///   }
  /// );
  /// # }
  /// ```
  pub fn query <A, B> (object_a : &A, object_b : &B) -> Self where
    A : object::Bounded, B : object::Bounded
  {
    let component::Position (ref position_a) = object_a.position();
    let component::Position (ref position_b) = object_b.position();
    let component::Bound    (ref shape_a)    = object_a.bound();
    let component::Bound    (ref shape_b)    = object_b.bound();

    match (shape_a, shape_b) {
      //
      // sphere vs. sphere
      //
      ( shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_a)),
        shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_b))
      ) => Self::query_sphere_sphere (
        position_a, sphere_a, position_b, sphere_b),
      //
      // capsule vs. capsule
      //
      ( shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_a)),
        shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_b))
      ) => Self::query_capsule_capsule (
        position_a, capsule_a, position_b, capsule_b),
      //
      // cuboid vs. cuboid
      //
      ( shape::Variant::Bounded (shape::Bounded::Cuboid (ref cuboid_a)),
        shape::Variant::Bounded (shape::Bounded::Cuboid (ref cuboid_b))
      ) => Self::query_cuboid_cuboid (
        position_a, cuboid_a, position_b, cuboid_b),
      //
      // sphere vs. capsule
      //
      ( shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_a)),
        shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_b))
      ) => Self::query_sphere_capsule (
        position_a, sphere_a, position_b, capsule_b),
      ( shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_a)),
        shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_b))
      ) => {
        let mut proximity = Self::query_sphere_capsule (
          position_b, sphere_b, position_a, capsule_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      //
      // sphere vs. cuboid
      //
      ( shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_a)),
        shape::Variant::Bounded (shape::Bounded::Cuboid (ref cuboid_b))
      ) => Self::query_sphere_cuboid (
        position_a, sphere_a, position_b, cuboid_b),
      ( shape::Variant::Bounded (shape::Bounded::Cuboid (ref cuboid_a)),
        shape::Variant::Bounded (shape::Bounded::Sphere (ref sphere_b))
      ) => {
        let mut proximity = Self::query_sphere_cuboid (
          position_b, sphere_b, position_a, cuboid_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      //
      // sphere vs. orthant
      //
      ( shape::Variant::Bounded   (shape::Bounded::Sphere    (ref sphere_a)),
        shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_b))
      ) => Self::query_sphere_orthant (
        position_a, sphere_a, position_b, orthant_b),
      ( shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_a)),
        shape::Variant::Bounded   (shape::Bounded::Sphere    (ref sphere_b))
      ) => {
        let mut proximity = Self::query_sphere_orthant (
          position_b, sphere_b, position_a, orthant_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      //
      // capsule vs. cuboid
      //
      ( shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_a)),
        shape::Variant::Bounded (shape::Bounded::Cuboid  (ref cuboid_b))
      ) => Self::query_capsule_cuboid (
        position_a, capsule_a, position_b, cuboid_b),
      ( shape::Variant::Bounded (shape::Bounded::Cuboid  (ref cuboid_a)),
        shape::Variant::Bounded (shape::Bounded::Capsule (ref capsule_b))
      ) => {
        let mut proximity = Self::query_capsule_cuboid (
          position_b, capsule_b, position_a, cuboid_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      //
      // capsule vs. orthant
      //
      ( shape::Variant::Bounded   (shape::Bounded::Capsule   (ref capsule_a)),
        shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_b))
      ) => Self::query_capsule_orthant (
        position_a, capsule_a, position_b, orthant_b),
      ( shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_a)),
        shape::Variant::Bounded   (shape::Bounded::Capsule   (ref capsule_b))
      ) => {
        let mut proximity = Self::query_capsule_orthant (
          position_b, capsule_b, position_a, orthant_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      //
      // cuboid vs. orthant
      //
      ( shape::Variant::Bounded   (shape::Bounded::Cuboid    (ref cuboid_a)),
        shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_b))
      ) => Self::query_cuboid_orthant (
        position_a, cuboid_a, position_b, orthant_b),
      ( shape::Variant::Unbounded (shape::Unbounded::Orthant (ref orthant_a)),
        shape::Variant::Bounded   (shape::Bounded::Cuboid    (ref cuboid_b))
      ) => {
        let mut proximity = Self::query_cuboid_orthant (
          position_b, cuboid_b, position_a, orthant_a);
        proximity.half_axis *= -1.0;
        proximity.normal    = proximity.normal.invert();
        proximity
      }
      _ => unimplemented!("proximity query not implemented for (A, B): {:?}",
        (shape_a, shape_b))
    }
  }

  pub fn relation (&self) -> Relation {
    if self.distance < 0.0 {
      Relation::Intersect
    } else if self.distance <= collision::CONTACT_DISTANCE {
      Relation::Contact
    } else {
      debug_assert!(self.distance > collision::CONTACT_DISTANCE);
      Relation::Disjoint
    }
  }

  #[inline]
  pub fn constraint_planar (&self) -> constraint::Planar {
    constraint::Planar::new (self.midpoint, self.normal)
  }

  pub fn query_sphere_sphere (
    position_a : &math::Point3 <f64>, sphere_a : &shape::Sphere <f64>,
    position_b : &math::Point3 <f64>, sphere_b : &shape::Sphere <f64>
  ) -> Self {
    use math::num_traits::Zero;
    let (radius_a, radius_b) = (*sphere_a.radius, *sphere_b.radius);
    let distance_normal_half_axis = |axis : &math::Vector3 <f64>| {
      let axis_distance = axis.magnitude();
      let distance      = axis_distance - radius_a - radius_b;
      let normal        = if axis.is_zero() {
        math::Unit3::axis_x()
      } else {
        math::Unit3::unchecked_approx (-(axis / axis_distance))
      };
      let half_axis     = -0.5 * distance * *normal;
      (distance, normal, half_axis)
    };
    let axis     = position_b - position_a;
    let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
    let midpoint = position_a - radius_a * *normal + half_axis;
    Proximity { distance, half_axis, midpoint, normal }
  }

  pub fn query_capsule_capsule (
    position_a : &math::Point3 <f64>, capsule_a : &shape::Capsule <f64>,
    position_b : &math::Point3 <f64>, capsule_b : &shape::Capsule <f64>
  ) -> Self {
    use math::num_traits::Zero;
    let (half_height_a, half_height_b) =
      (*capsule_a.half_height, *capsule_b.half_height);
    let (radius_a, radius_b) = (*capsule_a.radius, *capsule_b.radius);
    let shaft_max_a = position_a + math::Vector3::new (0.0, 0.0, half_height_a);
    let shaft_min_a = position_a - math::Vector3::new (0.0, 0.0, half_height_a);
    let shaft_max_b = position_b + math::Vector3::new (0.0, 0.0, half_height_b);
    let shaft_min_b = position_b - math::Vector3::new (0.0, 0.0, half_height_b);
    let distance_normal_half_axis = |axis : &math::Vector3 <f64>| {
      let axis_distance = axis.magnitude();
      let distance      = axis_distance - radius_a - radius_b;
      let normal        = if axis.is_zero() {
        math::Unit3::axis_x()
      } else {
        math::Unit3::unchecked_approx (-(axis / axis_distance))
      };
      let half_axis     = -0.5 * distance * *normal;
      (distance, normal, half_axis)
    };
    if shaft_max_a.0.z <= shaft_min_b.0.z {
      // cylinder shaft of B is above cylinder shaft of A
      let axis     = shaft_min_b - shaft_max_a;
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = shaft_max_a - radius_a * *normal + half_axis;
      Proximity { distance, half_axis, midpoint, normal }
    } else if shaft_max_b.0.z <= shaft_min_a.0.z {
      // cylinder shaft of B is below cylinder shaft of A
      let axis     = shaft_max_b.0 - shaft_min_a.0;
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = shaft_min_a - radius_a * *normal + half_axis;
      Proximity { distance, half_axis, midpoint, normal }
    } else {
      // cylinder shafts are adjacent
      let axis     =
        math::Point3::from ([position_b.0.x, position_b.0.y, 0.0]) -
        math::Point3::from ([position_a.0.x, position_a.0.y, 0.0]);
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = {
        let max_z  = f64::min (shaft_max_a.0.z, shaft_max_b.0.z);
        let min_z  = f64::max (shaft_min_a.0.z, shaft_min_b.0.z);
        let mid_z  = 0.5 * (max_z + min_z);
        let mid_a  = position_a - radius_a * *normal + half_axis;
        [mid_a.0.x, mid_a.0.y, mid_z].into()
      };
      Proximity { distance, half_axis, midpoint, normal }
    }
  }

  pub fn query_cuboid_cuboid (
    position_a : &math::Point3 <f64>, cuboid_a : &shape::Cuboid <f64>,
    position_b : &math::Point3 <f64>, cuboid_b : &shape::Cuboid <f64>
  ) -> Self {
    use math::VectorSpace;
    use math::num_traits::Zero;
    fn nearest_ab_to_distance (
      nearest_a      : &math::Point3  <f64>,
      nearest_b      : &math::Point3  <f64>,
      default_normal : &math::Unit3 <f64>
    ) -> Proximity {
      let axis      = nearest_b - nearest_a;
      let distance  = axis.magnitude();
      let half_axis = 0.5 * (nearest_b - nearest_a);
      let midpoint  = nearest_a + half_axis;
      let normal    = if axis.is_zero() {
        *default_normal
      } else {
        math::Unit3::unchecked_approx (-axis / distance)
      };
      Proximity { distance, half_axis, midpoint, normal }
    }

    let max_a = position_a + cuboid_a.max().0;
    let min_a = position_a + cuboid_a.min().0;
    let max_b = position_b + cuboid_b.max().0;
    let min_b = position_b + cuboid_b.min().0;
    let overlap_x = min_b.0.x < max_a.0.x && min_a.0.x < max_b.0.x;
    let overlap_y = min_b.0.y < max_a.0.y && min_a.0.y < max_b.0.y;
    let overlap_z = min_b.0.z < max_a.0.z && min_a.0.z < max_b.0.z;
    match (overlap_x, overlap_y, overlap_z) {
      (false, false, false) => {
        // no overlaps: nearest features are corners
        let a_to_b = position_b - position_a;
        debug_assert!(!a_to_b.x.is_zero());
        debug_assert!(!a_to_b.y.is_zero());
        debug_assert!(!a_to_b.z.is_zero());
        let a_to_b_sigvec = a_to_b.map (f64::signum);
        let nearest_a = position_a + cuboid_a.max().0 * a_to_b_sigvec;
        let nearest_b = position_b + cuboid_b.max().0 * (-a_to_b_sigvec);
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &math::Unit3::normalize (-a_to_b_sigvec))
      }
      (true, false, false)  => {
        // overlap x only: nearest features are edges
        let max_x  = f64::min (max_a.0.x, max_b.0.x);
        let min_x  = f64::max (min_a.0.x, min_b.0.x);
        debug_assert_ne!(min_x, max_x);
        let mid_x  = 0.5 * (min_x + max_x);
        let a_to_b = position_b - position_a;
        let a_to_b_yz = math::Vector3::new (0.0, a_to_b.y, a_to_b.z);
        let a_to_b_yz_sigvec = math::Vector3::sigvec (a_to_b_yz);
        debug_assert!(a_to_b_yz_sigvec.x.is_zero());
        debug_assert!(!a_to_b_yz_sigvec.y.is_zero());
        debug_assert!(!a_to_b_yz_sigvec.z.is_zero());
        let nearest_a = {
          let y = *cuboid_a.half_extent_y * a_to_b_yz_sigvec.y;
          let z = *cuboid_a.half_extent_z * a_to_b_yz_sigvec.z;
          [mid_x, position_a.0.y + y, position_a.0.z + z].into()
        };
        let nearest_b = {
          let y = -*cuboid_b.half_extent_y * a_to_b_yz_sigvec.y;
          let z = -*cuboid_b.half_extent_z * a_to_b_yz_sigvec.z;
          [mid_x, position_b.0.y + y, position_b.0.z + z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &math::Unit3::normalize (-a_to_b_yz_sigvec))
      }
      (false, true, false)  => {
        // overlap y only: nearest features are edges
        let max_y  = f64::min (max_a.0.y, max_b.0.y);
        let min_y  = f64::max (min_a.0.y, min_b.0.y);
        debug_assert_ne!(min_y, max_y);
        let mid_y  = 0.5 * (min_y + max_y);
        let a_to_b = position_b - position_a;
        let a_to_b_xz = math::Vector3::new (a_to_b.x, 0.0, a_to_b.z);
        let a_to_b_xz_sigvec = math::Vector3::sigvec (a_to_b_xz);
        debug_assert!(!a_to_b_xz_sigvec.x.is_zero());
        debug_assert!(a_to_b_xz_sigvec.y.is_zero());
        debug_assert!(!a_to_b_xz_sigvec.z.is_zero());
        let nearest_a = {
          let x = *cuboid_a.half_extent_x * a_to_b_xz_sigvec.x;
          let z = *cuboid_a.half_extent_z * a_to_b_xz_sigvec.z;
          [position_a.0.x + x, mid_y, position_a.0.z + z].into()
        };
        let nearest_b = {
          let x = -*cuboid_b.half_extent_x * a_to_b_xz_sigvec.x;
          let z = -*cuboid_b.half_extent_z * a_to_b_xz_sigvec.z;
          [position_b.0.x + x, mid_y, position_b.0.z + z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &math::Unit3::normalize (-a_to_b_xz_sigvec))
      }
      (false, false, true)  => {
        // overlap z only: nearest features are edges
        let max_z  = f64::min (max_a.0.z, max_b.0.z);
        let min_z  = f64::max (min_a.0.z, min_b.0.z);
        debug_assert_ne!(min_z, max_z);
        let mid_z  = 0.5 * (min_z + max_z);
        let a_to_b = position_b - position_a;
        let a_to_b_xy = math::Vector3::new (a_to_b.x, a_to_b.y, 0.0);
        let a_to_b_xy_sigvec = math::Vector3::sigvec (a_to_b_xy);
        debug_assert!(!a_to_b_xy_sigvec.x.is_zero());
        debug_assert!(!a_to_b_xy_sigvec.y.is_zero());
        debug_assert!(a_to_b_xy_sigvec.z.is_zero());
        let nearest_a = {
          let x = *cuboid_a.half_extent_x * a_to_b_xy_sigvec.x;
          let y = *cuboid_a.half_extent_y * a_to_b_xy_sigvec.y;
          [position_a.0.x + x, position_a.0.y + y, mid_z].into()
        };
        let nearest_b = {
          let x = -*cuboid_b.half_extent_x * a_to_b_xy_sigvec.x;
          let y = -*cuboid_b.half_extent_y * a_to_b_xy_sigvec.y;
          [position_b.0.x + x, position_b.0.y + y, mid_z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &math::Unit3::normalize (-a_to_b_xy_sigvec))
      }
      (true, true, false)   => {
        // overlap x and y: nearest features are faces
        let max_x    = f64::min (max_a.0.x, max_b.0.x);
        let min_x    = f64::max (min_a.0.x, min_b.0.x);
        let mid_x    = 0.5 * (min_x + max_x);
        let max_y    = f64::min (max_a.0.y, max_b.0.y);
        let min_y    = f64::max (min_a.0.y, min_b.0.y);
        let mid_y    = 0.5 * (min_y + max_y);
        let a_to_b_z = position_b.0.z - position_a.0.z;
        let a_to_b_signum_z = a_to_b_z.signum();
        debug_assert_ne!(a_to_b_signum_z, 0.0);
        let nearest_a = math::Point3::new (
          mid_x, mid_y,
          position_a.0.z + a_to_b_signum_z * *cuboid_a.half_extent_z
        );
        let nearest_b = math::Point3::new (
          mid_x, mid_y,
          position_b.0.z - a_to_b_signum_z * *cuboid_b.half_extent_z
        );
        let nearest_a_to_b_z = nearest_b.0.z - nearest_a.0.z;
        let distance  = nearest_a_to_b_z.abs();
        let half_axis = [0.0, 0.0, 0.5 * nearest_a_to_b_z].into();
        let normal    = math::Unit3::unchecked (
          [0.0, 0.0, -a_to_b_signum_z].into());
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (true, false, true)   => {
        // overlap x and z: nearest features are faces
        let max_x    = f64::min (max_a.0.x, max_b.0.x);
        let min_x    = f64::max (min_a.0.x, min_b.0.x);
        let mid_x    = 0.5 * (min_x + max_x);
        let max_z    = f64::min (max_a.0.z, max_b.0.z);
        let min_z    = f64::max (min_a.0.z, min_b.0.z);
        let mid_z    = 0.5 * (min_z + max_z);
        let a_to_b_y = position_b.0.y - position_a.0.y;
        let a_to_b_signum_y = a_to_b_y.signum();
        debug_assert_ne!(a_to_b_signum_y, 0.0);
        let nearest_a = math::Point3::new (
          mid_x,
          position_a.0.y + a_to_b_signum_y * *cuboid_a.half_extent_y,
          mid_z
        );
        let nearest_b = math::Point3::new (
          mid_x,
          position_b.0.y - a_to_b_signum_y * *cuboid_b.half_extent_y,
          mid_z
        );
        let nearest_a_to_b_y = nearest_b.0.y - nearest_a.0.y;
        let distance  = nearest_a_to_b_y.abs();
        let half_axis = [0.0, 0.5 * nearest_a_to_b_y, 0.0].into();
        let normal    = math::Unit3::unchecked (
          [0.0, -a_to_b_signum_y, 0.0].into());
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (false, true, true)   => {
        // overlap y and z: nearest features are faces
        let max_y    = f64::min (max_a.0.y, max_b.0.y);
        let min_y    = f64::max (min_a.0.y, min_b.0.y);
        let mid_y    = 0.5 * (min_y + max_y);
        let max_z    = f64::min (max_a.0.z, max_b.0.z);
        let min_z    = f64::max (min_a.0.z, min_b.0.z);
        let mid_z    = 0.5 * (min_z + max_z);
        let a_to_b_x = position_b.0.x - position_a.0.x;
        let a_to_b_signum_x = a_to_b_x.signum();
        debug_assert_ne!(a_to_b_signum_x, 0.0);
        let nearest_a = math::Point3::new (
          position_a.0.x + a_to_b_signum_x * *cuboid_a.half_extent_x,
          mid_y,
          mid_z
        );
        let nearest_b = math::Point3::new (
          position_b.0.x - a_to_b_signum_x * *cuboid_b.half_extent_x,
          mid_y,
          mid_z
        );
        let nearest_a_to_b_x = nearest_b.0.x - nearest_a.0.x;
        let distance  = nearest_a_to_b_x.abs();
        let half_axis = [0.5 * nearest_a_to_b_x, 0.0, 0.0].into();
        let normal    = math::Unit3::unchecked (
          [-a_to_b_signum_x, 0.0, 0.0].into());
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (true, true, true)    => {
        // overlap on all axes: intersection
        let max_x     = f64::min (max_a.0.x, max_b.0.x);
        let min_x     = f64::max (min_a.0.x, min_b.0.x);
        let mid_x     = 0.5 * (min_x + max_x);
        let max_y     = f64::min (max_a.0.y, max_b.0.y);
        let min_y     = f64::max (min_a.0.y, min_b.0.y);
        let mid_y     = 0.5 * (min_y + max_y);
        let max_z     = f64::min (max_a.0.z, max_b.0.z);
        let min_z     = f64::max (min_a.0.z, min_b.0.z);
        let mid_z     = 0.5 * (min_z + max_z);
        let midpoint  = [mid_x, mid_y, mid_z].into();
        let resolve_x = if position_a.0.x < position_b.0.x {
          min_b.0.x - max_a.0.x
        } else {
          debug_assert!(position_a.0.x >= position_b.0.x);
          max_b.0.x - min_a.0.x
        };
        let resolve_y = if position_a.0.y < position_b.0.y {
          min_b.0.y - max_a.0.y
        } else {
          debug_assert!(position_a.0.y >= position_b.0.y);
          max_b.0.y - min_a.0.y
        };
        let resolve_z = if position_a.0.z < position_b.0.z {
          min_b.0.z - max_a.0.z
        } else {
          debug_assert!(position_a.0.z >= position_b.0.z);
          max_b.0.z - min_a.0.z
        };
        let resolve_x_abs = resolve_x.abs();
        let resolve_y_abs = resolve_y.abs();
        let resolve_z_abs = resolve_z.abs();
        let (distance, half_axis, normal) : (
          f64, math::Vector3 <f64>, math::Unit3 <f64>
        ) = if resolve_x_abs <= resolve_y_abs && resolve_x_abs <= resolve_z_abs {
          ( -resolve_x_abs,
            0.5 * math::Vector3::new (resolve_x, 0.0, 0.0),
            math::Unit3::unchecked ([resolve_x.signum(), 0.0, 0.0].into())
          )
        } else if
          resolve_y_abs <= resolve_x_abs && resolve_y_abs <= resolve_z_abs
        {
          ( -resolve_y_abs,
            0.5 * math::Vector3::new (0.0, resolve_y, 0.0),
            math::Unit3::unchecked ([0.0, resolve_y.signum(), 0.0].into())
          )
        } else {
          debug_assert!(resolve_z_abs <= resolve_x_abs);
          debug_assert!(resolve_z_abs <= resolve_y_abs);
          ( -resolve_z_abs,
            0.5 * math::Vector3::new (0.0, 0.0, resolve_z),
            math::Unit3::unchecked ([0.0, 0.0, resolve_z.signum()].into())
          )
        };
        debug_assert!(!half_axis.is_zero());
        Proximity { distance, half_axis, midpoint, normal }
      }
    }
  }

  /*
  /// Uses pre-computed sigvec normals with array lookup.
  ///
  /// This benchmarked 2-3ns slower than the version using `normalize`.
  pub fn query_cuboid_cuboid_array (
    position_a : &math::Point3 <f64>, cuboid_a : &shape::Cuboid <f64>,
    position_b : &math::Point3 <f64>, cuboid_b : &shape::Cuboid <f64>
  ) -> Self {
    use math::{EuclideanSpace, InnerSpace, Zero};

    fn nearest_ab_to_distance (
      nearest_a      : &math::Point3  <f64>,
      nearest_b      : &math::Point3  <f64>,
      default_normal : &math::Vector3 <f64>
    ) -> Proximity {
      let axis      = nearest_b - nearest_a;
      let distance  = axis.magnitude();
      let half_axis = 0.5 * (nearest_b - nearest_a);
      let midpoint  = nearest_a + half_axis;
      let normal    = if axis.is_zero() {
        *default_normal
      } else {
        -axis / distance
      };
      Proximity { distance, half_axis, midpoint, normal }
    }

    let max_a = position_a + cuboid_a.max().0;
    let min_a = position_a + cuboid_a.min().0;
    let max_b = position_b + cuboid_b.max().0;
    let min_b = position_b + cuboid_b.min().0;
    let overlap_x = min_b.0.x < max_a.0.x && min_a.0.x < max_b.0.x;
    let overlap_y = min_b.0.y < max_a.0.y && min_a.0.y < max_b.0.y;
    let overlap_z = min_b.0.z < max_a.0.z && min_a.0.z < max_b.0.z;
    match (overlap_x, overlap_y, overlap_z) {
      (false, false, false) => {
        use math::ElementWise;
        // no overlaps: nearest features are corners
        let a_to_b = position_b - position_a;
        debug_assert!(!a_to_b.x.is_zero());
        debug_assert!(!a_to_b.y.is_zero());
        debug_assert!(!a_to_b.z.is_zero());
        let a_to_b_sigvec = a_to_b.map (f64::signum);
        let nearest_a = position_a +
          cuboid_a.half_extents().mul_element_wise (a_to_b_sigvec);
        let nearest_b = position_b +
          cuboid_b.half_extents().mul_element_wise (-a_to_b_sigvec);
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &-math::sigvec_unit_f64_unchecked (a_to_b_sigvec))
      }
      (true, false, false)  => {
        // overlap x only: nearest features are edges
        let max_x  = f64::min (max_a.0.x, max_b.0.x);
        let min_x  = f64::max (min_a.0.x, min_b.0.x);
        debug_assert_ne!(min_x, max_x);
        let mid_x  = 0.5 * (min_x + max_x);
        let a_to_b = position_b - position_a;
        let a_to_b_yz = math::Vector3::new (0.0, a_to_b.y, a_to_b.z);
        let a_to_b_yz_sigvec = math::sigvec (a_to_b_yz);
        debug_assert!(a_to_b_yz_sigvec.x.is_zero());
        debug_assert!(!a_to_b_yz_sigvec.y.is_zero());
        debug_assert!(!a_to_b_yz_sigvec.z.is_zero());
        let nearest_a = {
          let y = cuboid_a.half_extent_y * a_to_b_yz_sigvec.y;
          let z = cuboid_a.half_extent_z * a_to_b_yz_sigvec.z;
          [mid_x, position_a.0.y + y, position_a.0.z + z].into()
        };
        let nearest_b = {
          let y = -cuboid_b.half_extent_y * a_to_b_yz_sigvec.y;
          let z = -cuboid_b.half_extent_z * a_to_b_yz_sigvec.z;
          [mid_x, position_b.0.y + y, position_b.0.z + z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &-math::sigvec_unit_f64_unchecked (a_to_b_yz_sigvec))
      }
      (false, true, false)  => {
        // overlap y only: nearest features are edges
        let max_y  = f64::min (max_a.0.y, max_b.0.y);
        let min_y  = f64::max (min_a.0.y, min_b.0.y);
        debug_assert_ne!(min_y, max_y);
        let mid_y  = 0.5 * (min_y + max_y);
        let a_to_b = position_b - position_a;
        let a_to_b_xz = math::Vector3::new (a_to_b.x, 0.0, a_to_b.z);
        let a_to_b_xz_sigvec = math::sigvec (a_to_b_xz);
        debug_assert!(!a_to_b_xz_sigvec.x.is_zero());
        debug_assert!(a_to_b_xz_sigvec.y.is_zero());
        debug_assert!(!a_to_b_xz_sigvec.z.is_zero());
        let nearest_a = {
          let x = cuboid_a.half_extent_x * a_to_b_xz_sigvec.x;
          let z = cuboid_a.half_extent_z * a_to_b_xz_sigvec.z;
          [position_a.0.x + x, mid_y, position_a.0.z + z].into()
        };
        let nearest_b = {
          let x = -cuboid_b.half_extent_x * a_to_b_xz_sigvec.x;
          let z = -cuboid_b.half_extent_z * a_to_b_xz_sigvec.z;
          [position_b.0.x + x, mid_y, position_b.0.z + z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &-math::sigvec_unit_f64_unchecked (a_to_b_xz_sigvec))
      }
      (false, false, true)  => {
        // overlap z only: nearest features are edges
        let max_z  = f64::min (max_a.0.z, max_b.0.z);
        let min_z  = f64::max (min_a.0.z, min_b.0.z);
        debug_assert_ne!(min_z, max_z);
        let mid_z  = 0.5 * (min_z + max_z);
        let a_to_b = position_b - position_a;
        let a_to_b_xy = math::Vector3::new (a_to_b.x, a_to_b.y, 0.0);
        let a_to_b_xy_sigvec = math::sigvec (a_to_b_xy);
        debug_assert!(!a_to_b_xy_sigvec.x.is_zero());
        debug_assert!(!a_to_b_xy_sigvec.y.is_zero());
        debug_assert!(a_to_b_xy_sigvec.z.is_zero());
        let nearest_a = {
          let x = cuboid_a.half_extent_x * a_to_b_xy_sigvec.x;
          let y = cuboid_a.half_extent_y * a_to_b_xy_sigvec.y;
          [position_a.0.x + x, position_a.0.y + y, mid_z].into()
        };
        let nearest_b = {
          let x = -cuboid_b.half_extent_x * a_to_b_xy_sigvec.x;
          let y = -cuboid_b.half_extent_y * a_to_b_xy_sigvec.y;
          [position_b.0.x + x, position_b.0.y + y, mid_z].into()
        };
        nearest_ab_to_distance (&nearest_a, &nearest_b,
          &-math::sigvec_unit_f64_unchecked (a_to_b_xy_sigvec))
      }
      (true, true, false)   => {
        // overlap x and y: nearest features are faces
        let max_x    = f64::min (max_a.0.x, max_b.0.x);
        let min_x    = f64::max (min_a.0.x, min_b.0.x);
        let mid_x    = 0.5 * (min_x + max_x);
        let max_y    = f64::min (max_a.0.y, max_b.0.y);
        let min_y    = f64::max (min_a.0.y, min_b.0.y);
        let mid_y    = 0.5 * (min_y + max_y);
        let a_to_b_z = position_b.0.z - position_a.0.z;
        let a_to_b_signum_z = a_to_b_z.signum();
        debug_assert_ne!(a_to_b_signum_z, 0.0);
        let nearest_a = math::Point3::new (
          mid_x, mid_y,
          position_a.0.z + a_to_b_signum_z * cuboid_a.half_extent_z
        );
        let nearest_b = math::Point3::new (
          mid_x, mid_y,
          position_b.0.z - a_to_b_signum_z * cuboid_b.half_extent_z
        );
        let nearest_a_to_b_z = nearest_b.0.z - nearest_a.0.z;
        let distance  = nearest_a_to_b_z.abs();
        let half_axis = [0.0, 0.0, 0.5 * nearest_a_to_b_z].into();
        let normal    = [0.0, 0.0, -a_to_b_signum_z].into();
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (true, false, true)   => {
        // overlap x and z: nearest features are faces
        let max_x    = f64::min (max_a.0.x, max_b.0.x);
        let min_x    = f64::max (min_a.0.x, min_b.0.x);
        let mid_x    = 0.5 * (min_x + max_x);
        let max_z    = f64::min (max_a.0.z, max_b.0.z);
        let min_z    = f64::max (min_a.0.z, min_b.0.z);
        let mid_z    = 0.5 * (min_z + max_z);
        let a_to_b_y = position_b.0.y - position_a.0.y;
        let a_to_b_signum_y = a_to_b_y.signum();
        debug_assert_ne!(a_to_b_signum_y, 0.0);
        let nearest_a = math::Point3::new (
          mid_x,
          position_a.0.y + a_to_b_signum_y * cuboid_a.half_extent_y,
          mid_z
        );
        let nearest_b = math::Point3::new (
          mid_x,
          position_b.0.y - a_to_b_signum_y * cuboid_b.half_extent_y,
          mid_z
        );
        let nearest_a_to_b_y = nearest_b.0.y - nearest_a.0.y;
        let distance  = nearest_a_to_b_y.abs();
        let half_axis = [0.0, 0.5 * nearest_a_to_b_y, 0.0].into();
        let normal    = [0.0, -a_to_b_signum_y, 0.0].into();
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (false, true, true)   => {
        // overlap y and z: nearest features are faces
        let max_y    = f64::min (max_a.0.y, max_b.0.y);
        let min_y    = f64::max (min_a.0.y, min_b.0.y);
        let mid_y    = 0.5 * (min_y + max_y);
        let max_z    = f64::min (max_a.0.z, max_b.0.z);
        let min_z    = f64::max (min_a.0.z, min_b.0.z);
        let mid_z    = 0.5 * (min_z + max_z);
        let a_to_b_x = position_b.0.x - position_a.0.x;
        let a_to_b_signum_x = a_to_b_x.signum();
        debug_assert_ne!(a_to_b_signum_x, 0.0);
        let nearest_a = math::Point3::new (
          position_a.0.x + a_to_b_signum_x * cuboid_a.half_extent_x,
          mid_y,
          mid_z
        );
        let nearest_b = math::Point3::new (
          position_b.0.x - a_to_b_signum_x * cuboid_b.half_extent_x,
          mid_y,
          mid_z
        );
        let nearest_a_to_b_x = nearest_b.0.x - nearest_a.0.x;
        let distance  = nearest_a_to_b_x.abs();
        let half_axis = [0.5 * nearest_a_to_b_x, 0.0, 0.0].into();
        let normal    = [-a_to_b_signum_x, 0.0, 0.0].into();
        let midpoint  = nearest_a + half_axis;
        Proximity { distance, half_axis, midpoint, normal }
      }
      (true, true, true)    => {
        // overlap on all axes: intersection
        let max_x     = f64::min (max_a.0.x, max_b.0.x);
        let min_x     = f64::max (min_a.0.x, min_b.0.x);
        let mid_x     = 0.5 * (min_x + max_x);
        let max_y     = f64::min (max_a.0.y, max_b.0.y);
        let min_y     = f64::max (min_a.0.y, min_b.0.y);
        let mid_y     = 0.5 * (min_y + max_y);
        let max_z     = f64::min (max_a.0.z, max_b.0.z);
        let min_z     = f64::max (min_a.0.z, min_b.0.z);
        let mid_z     = 0.5 * (min_z + max_z);
        let midpoint  = [mid_x, mid_y, mid_z].into();
        let resolve_x = if position_a.0.x < position_b.0.x {
          min_b.0.x - max_a.0.x
        } else {
          debug_assert!(position_a.0.x >= position_b.0.x);
          max_b.0.x - min_a.0.x
        };
        let resolve_y = if position_a.0.y < position_b.0.y {
          min_b.0.y - max_a.0.y
        } else {
          debug_assert!(position_a.0.y >= position_b.0.y);
          max_b.0.y - min_a.0.y
        };
        let resolve_z = if position_a.0.z < position_b.0.z {
          min_b.0.z - max_a.0.z
        } else {
          debug_assert!(position_a.0.z >= position_b.0.z);
          max_b.0.z - min_a.0.z
        };
        let resolve_x_abs = resolve_x.abs();
        let resolve_y_abs = resolve_y.abs();
        let resolve_z_abs = resolve_z.abs();
        let (distance, half_axis, normal) : (
          f64, math::Vector3 <f64>, math::Vector3 <f64>
        ) = if resolve_x_abs <= resolve_y_abs && resolve_x_abs <= resolve_z_abs {
          ( -resolve_x_abs,
            0.5 * math::Vector3::new (resolve_x, 0.0, 0.0),
            [resolve_x.signum(), 0.0, 0.0].into()
          )
        } else if
          resolve_y_abs <= resolve_x_abs && resolve_y_abs <= resolve_z_abs
        {
          ( -resolve_y_abs,
            0.5 * math::Vector3::new (0.0, resolve_y, 0.0),
            [0.0, resolve_y.signum(), 0.0].into()
          )
        } else {
          debug_assert!(resolve_z_abs <= resolve_x_abs);
          debug_assert!(resolve_z_abs <= resolve_y_abs);
          ( -resolve_z_abs,
            0.5 * math::Vector3::new (0.0, 0.0, resolve_z),
            [0.0, 0.0, resolve_z.signum()].into()
          )
        };
        debug_assert!(!half_axis.is_zero());
        Proximity { distance, half_axis, midpoint, normal }
      }
    }
  } // end query_cuboid_cuboid_array

  /// This is a version of the cuboid distance query with the three edge
  /// cases factored out into a function taking vector indices. This turns out
  /// to be slower in benchmarks (190ns vs. the 180ns inline versions above).
  pub fn query_cuboid_cuboid_refactor (
    position_a : &math::Point3 <f64>, cuboid_a : &shape::Cuboid <f64>,
    position_b : &math::Point3 <f64>, cuboid_b : &shape::Cuboid <f64>
  ) -> Self {
    use math::{EuclideanSpace, InnerSpace, Zero};

    #[inline]
    fn distance_edge (
      edge : usize, other1 : usize, other2 : usize,
      position_a : &math::Point3 <f64>, cuboid_a : &shape::Cuboid <f64>,
      position_b : &math::Point3 <f64>, cuboid_b : &shape::Cuboid <f64>,
      max_a      : &math::Point3 <f64>, min_a    : &math::Point3 <f64>,
      max_b      : &math::Point3 <f64>, min_b    : &math::Point3 <f64>
    ) -> Proximity {
      let max_edge  = f64::min (max_a[edge], max_b[edge]);
      let min_edge  = f64::max (min_a[edge], min_b[edge]);
      debug_assert_ne!(min_edge, max_edge);
      let mid_edge  = 0.5 * (min_edge + max_edge);
      let a_to_b = position_b - position_a;
      let a_to_b_others = {
        let mut a_to_b_others = a_to_b;
        a_to_b_others[edge] = 0.0;
        a_to_b_others
      };
      let a_to_b_others_signum = a_to_b_others.map (math::signum_or_zero);
      debug_assert!(a_to_b_others_signum[edge].is_zero());
      debug_assert!(!a_to_b_others_signum[other1].is_zero());
      debug_assert!(!a_to_b_others_signum[other2].is_zero());
      let nearest_a = {
        let other_component1 =
          cuboid_a.half_extents()[other1] * a_to_b_others_signum[other1];
        let other_component2 =
          cuboid_a.half_extents()[other2] * a_to_b_others_signum[other2];
        let mut nearest_a = [0.0; 3];
        nearest_a[edge]   = mid_edge;
        nearest_a[other1] = position_a[other1] + other_component1;
        nearest_a[other2] = position_a[other2] + other_component2;
        nearest_a.into()
      };
      let nearest_b = {
        let other_component1 =
          -cuboid_b.half_extents()[other1] * a_to_b_others_signum[other1];
        let other_component2 =
          -cuboid_b.half_extents()[other2] * a_to_b_others_signum[other2];
        let mut nearest_b = [0.0; 3];
        nearest_b[edge]   = mid_edge;
        nearest_b[other1] = position_b[other1] + other_component1;
        nearest_b[other2] = position_b[other2] + other_component2;
        nearest_b.into()
      };
      nearest_ab_to_distance (&nearest_a, &nearest_b, &a_to_b_others_signum)
    }

    fn nearest_ab_to_distance (
      nearest_a     : &math::Point3 <f64>,
      nearest_b     : &math::Point3 <f64>,
      a_to_b_signum : &math::Vector3 <f64>
    ) -> Proximity {
      let axis      = nearest_b - nearest_a;
      let distance  = axis.magnitude();
      let half_axis = 0.5 * (nearest_b - nearest_a);
      let midpoint  = nearest_a + half_axis;
      let normal    = if axis.is_zero() {
        -a_to_b_signum.normalize()
      } else {
        -axis / distance
      };
      Proximity { distance, half_axis, midpoint, normal }
    }

    let max_a = position_a + cuboid_a.max().0;
    let min_a = position_a + cuboid_a.min().0;
    let max_b = position_b + cuboid_b.max().0;
    let min_b = position_b + cuboid_b.min().0;

    let overlap_x = min_b.0.x < max_a.0.x && min_a.0.x < max_b.0.x;
    let overlap_y = min_b.0.y < max_a.0.y && min_a.0.y < max_b.0.y;
    let overlap_z = min_b.0.z < max_a.0.z && min_a.0.z < max_b.0.z;
    match (overlap_x, overlap_y, overlap_z) {
      (false, false, false) => {
        use math::ElementWise;
        // no overlaps: nearest features are corners
        let a_to_b = position_b - position_a;
        debug_assert!(!a_to_b.x.is_zero());
        debug_assert!(!a_to_b.y.is_zero());
        debug_assert!(!a_to_b.z.is_zero());
        let a_to_b_signum = a_to_b.map (f64::signum);
        let nearest_a = position_a +
          cuboid_a.half_extents().mul_element_wise (a_to_b_signum);
        let nearest_b = position_b +
          cuboid_b.half_extents().mul_element_wise (-a_to_b_signum);
        nearest_ab_to_distance (&nearest_a, &nearest_b, &a_to_b_signum)
      }
      (true, false, false)  => {
        // overlap x only: nearest features are edges
        distance_edge (0, 1, 2,
          position_a, cuboid_a, position_b, cuboid_b,
          &max_a, &min_a, &max_b, &min_b
        )
      }
      (false, true, false)  => {
        // overlap y only: nearest features are edges
        distance_edge (1, 0, 2,
          position_a, cuboid_a, position_b, cuboid_b,
          &max_a, &min_a, &max_b, &min_b
        )
      }
      (false, false, true)  => {
        // overlap z only: nearest features are edges
        distance_edge (2, 1, 2,
          position_a, cuboid_a, position_b, cuboid_b,
          &max_a, &min_a, &max_b, &min_b
        )
      }
      (true, true, false)   => {
        // overlap x and y: nearest features are faces
        unimplemented!()
      }
      (true, false, true)   => {
        // overlap x and z: nearest features are faces
        unimplemented!()
      }
      (false, true, true)   => {
        // overlap y and z: nearest features are faces
        unimplemented!()
      }
      (true, true, true)    => {
        // overlap on all axes: intersection
        unimplemented!()
      }
    }
  } // end query_cuboid_cuboid_refactor
  */

  pub fn query_sphere_capsule (
    position_a : &math::Point3 <f64>, sphere_a  : &shape::Sphere <f64>,
    position_b : &math::Point3 <f64>, capsule_b : &shape::Capsule <f64>
  ) -> Self {
    use math::num_traits::Zero;
    let half_height_b = *capsule_b.half_height;
    let (radius_a, radius_b) = (*sphere_a.radius, *capsule_b.radius);
    let shaft_max_b = position_b + math::Vector3::new (0.0, 0.0, half_height_b);
    let shaft_min_b = position_b - math::Vector3::new (0.0, 0.0, half_height_b);
    let distance_normal_half_axis = |axis : &math::Vector3 <f64>| {
      let axis_distance = axis.magnitude();
      let distance      = axis_distance - radius_a - radius_b;
      let normal        = if axis.is_zero() {
        math::Unit3::axis_x()
      } else {
        math::Unit3::unchecked_approx (-(axis / axis_distance))
      };
      let half_axis     = -0.5 * distance * *normal;
      (distance, normal, half_axis)
    };
    if position_a.0.z <= shaft_min_b.0.z {
      // cylinder shaft of B is above center of A
      let axis     = shaft_min_b - position_a;
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = position_a - radius_a * *normal + half_axis;
      Proximity { distance, half_axis, midpoint, normal }
    } else if shaft_max_b.0.z <= position_a.0.z {
      // cylinder shaft of B is below center of A
      let axis     = shaft_max_b - position_a;
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = position_a - radius_a * *normal + half_axis;
      Proximity { distance, half_axis, midpoint, normal }
    } else {
      // cylinder shaft of B is adjacent to center of A
      let axis     =
        math::Point3::from ([position_b.0.x, position_b.0.y, 0.0]) -
        math::Point3::from ([position_a.0.x, position_a.0.y, 0.0]);
      let (distance, normal, half_axis) = distance_normal_half_axis (&axis);
      let midpoint = {
        let max_z  = f64::min (position_a.0.z, shaft_max_b.0.z);
        let min_z  = f64::max (position_a.0.z, shaft_min_b.0.z);
        let mid_z  = 0.5 * (max_z + min_z);
        let mid_a  = position_a - radius_a * *normal + half_axis;
        [mid_a.0.x, mid_a.0.y, mid_z].into()
      };
      Proximity { distance, half_axis, midpoint, normal }
    }
  }

  pub fn query_sphere_cuboid (
    position_a : &math::Point3 <f64>, sphere_a : &shape::Sphere <f64>,
    position_b : &math::Point3 <f64>, cuboid_b : &shape::Cuboid <f64>
  ) -> Self {
    use math::num_traits::Zero;
    let cuboid_max_b = position_b + cuboid_b.max().0;
    let cuboid_min_b = position_b - cuboid_b.max().0;

    let overlap_x = cuboid_min_b.0.x < position_a.0.x &&
      position_a.0.x < cuboid_max_b.0.x;
    let overlap_y = cuboid_min_b.0.y < position_a.0.y &&
      position_a.0.y < cuboid_max_b.0.y;

    let sphere_a_radius        = *sphere_a.radius;
    let cuboid_b_half_extent_x = *cuboid_b.half_extent_x;
    let cuboid_b_half_extent_y = *cuboid_b.half_extent_y;
    let cuboid_b_half_extent_z = *cuboid_b.half_extent_z;
    if position_a.0.z >= cuboid_max_b.0.z {
      // sphere above cuboid
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest/deepest point is a corner of the cuboid
          let b_to_a        = position_a - position_b;
          debug_assert!(!b_to_a.x.is_zero());
          debug_assert!(!b_to_a.y.is_zero());
          debug_assert!(!b_to_a.z.is_zero());
          let b_to_a_sigvec = b_to_a.map (f64::signum);
          let nearest_b     = position_b + cuboid_b.max().0 * b_to_a_sigvec;
          let a_to_b        = nearest_b - position_a;
          let nearest_a     = position_a + if !a_to_b.is_zero() {
            sphere_a_radius * a_to_b.normalized()
          } else {
            [-b_to_a_sigvec.x * sphere_a_radius, 0.0, 0.0].into()
          };
          let axis          = nearest_b - nearest_a;
          let distance      = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis     = 0.5 * axis;
          let normal        = if axis.is_zero() {
            math::Unit3::normalize (b_to_a_sigvec)
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint      = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false) => {
          // nearest/deepest point is on an edge parallel to the X axis
          let signum_y  = (position_a.0.y - position_b.0.y).signum();
          let nearest_b = math::Point3::new (
            position_a.0.x,
            position_b.0.y + signum_y * cuboid_b_half_extent_y,
            cuboid_max_b.0.z
          );
          let a_to_b    = nearest_b - position_a;
          let nearest_a = position_a + if a_to_b.is_zero() {
            -signum_y * math::Vector3::new (0.0, sphere_a_radius, 0.0)
          } else {
            sphere_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (0.0, signum_y, 1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true) => {
          // nearest/deepest point is on an edge parallel to the Y axis
          let signum_x  = (position_a.0.x - position_b.0.x).signum();
          let nearest_b = math::Point3::new (
            position_b.0.x + signum_x * cuboid_b_half_extent_x,
            position_a.0.y,
            cuboid_max_b.0.z
          );
          let a_to_b    = nearest_b - position_a;
          let nearest_a = position_a + if a_to_b.is_zero() {
            -signum_x * math::Vector3::new (sphere_a_radius, 0.0, 0.0)
          } else {
            sphere_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (signum_x, 0.0, 1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // nearest/deepest point is in the -Z direction
          let nearest_a : math::Point3 <f64> =
            position_a - math::Vector3::new (0.0, 0.0, sphere_a_radius);
          let nearest_b : math::Point3 <f64> =
            [position_a.0.x, position_a.0.y, cuboid_max_b.0.z].into();
          let axis      = nearest_b - nearest_a;
          let normal    = math::Unit3::axis_z();
          let half_axis = 0.5 * axis;
          let distance  = nearest_a.0.z - nearest_b.0.z;
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
      }
    } else if position_a.0.z <= cuboid_min_b.0.z {
      // sphere below cuboid
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest/deepest point is a corner of the cuboid
          let b_to_a        = position_a - position_b;
          debug_assert!(!b_to_a.x.is_zero());
          debug_assert!(!b_to_a.y.is_zero());
          debug_assert!(!b_to_a.z.is_zero());
          let b_to_a_sigvec = b_to_a.map (f64::signum);
          let nearest_b     = position_b + cuboid_b.max().0 * b_to_a_sigvec;
          let a_to_b        = nearest_b - position_a;
          let nearest_a     = position_a + if !a_to_b.is_zero() {
            sphere_a_radius * a_to_b.normalized()
          } else {
            [-b_to_a_sigvec.x * sphere_a_radius, 0.0, 0.0].into()
          };
          let axis          = nearest_b - nearest_a;
          let distance      = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis     = 0.5 * axis;
          let normal        = if axis.is_zero() {
            math::Unit3::normalize (b_to_a_sigvec)
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint      = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false) => {
          // nearest/deepest point is on an edge parallel to the X axis
          let signum_y  = (position_a.0.y - position_b.0.y).signum();
          let nearest_b = math::Point3::new (
            position_a.0.x,
            position_b.0.y + signum_y * cuboid_b_half_extent_y,
            cuboid_min_b.0.z
          );
          let a_to_b    = nearest_b - position_a;
          let nearest_a = position_a + if a_to_b.is_zero() {
            -signum_y * math::Vector3::new (0.0, sphere_a_radius, 0.0)
          } else {
            sphere_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (0.0, signum_y, -1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true) => {
          // nearest/deepest point is on an edge parallel to the Y axis
          let signum_x  = (position_a.0.x - position_b.0.x).signum();
          let nearest_b = math::Point3::new (
            position_b.0.x + signum_x * cuboid_b_half_extent_x,
            position_a.0.y,
            cuboid_min_b.0.z
          );
          let a_to_b    = nearest_b - position_a;
          let nearest_a = position_a + if a_to_b.is_zero() {
            -signum_x * math::Vector3::new (sphere_a_radius, 0.0, 0.0)
          } else {
            sphere_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (signum_x, 0.0, -1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // nearest/deepest point is in the -Z direction
          let nearest_a : math::Point3 <f64> =
            position_a + math::Vector3::new (0.0, 0.0, sphere_a_radius);
          let nearest_b : math::Point3 <f64> =
            [position_a.0.x, position_a.0.y, cuboid_min_b.0.z].into();
          let axis      = nearest_b - nearest_a;
          let normal    = math::Unit3::axis_z().invert();
          let half_axis = 0.5 * axis;
          let distance  = nearest_b.0.z - nearest_a.0.z;
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
      }
    } else {
      // sphere center overlaps on z axis
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest point is on a Z parallel edge of the cuboid
          let b_to_a_signum_x = (position_a.0.x - position_b.0.x).signum();
          let b_to_a_signum_y = (position_a.0.y - position_b.0.y).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (b_to_a_signum_x, b_to_a_signum_y, 0.0)
              .normalized();
          let nearest_b = math::Point3::from ([
            position_b.0.x + b_to_a_signum_x * cuboid_b_half_extent_x,
            position_b.0.y + b_to_a_signum_y * cuboid_b_half_extent_y,
            position_a.0.z
          ]);
          let a_to_b    = math::Vector3::new (
            nearest_b.0.x - position_a.0.x,
            nearest_b.0.y - position_a.0.y,
            0.0
          );
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, position_a.0.z
          ) + sphere_a_radius * if !a_to_b.is_zero() {
            a_to_b.normalized()
          } else {
            -b_to_a_unit_sigvec
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false)  => {
          // nearest point is on a Z/X parallel face of the cuboid
          let b_to_a_signum_y    = (position_a.0.y - position_b.0.y).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (0.0, b_to_a_signum_y, 0.0);
          let nearest_b = math::Point3::from ([
            position_a.0.x,
            position_b.0.y + b_to_a_signum_y * cuboid_b_half_extent_y,
            position_a.0.z
          ]);
          let a_to_b    =
            math::Vector3::new (0.0, nearest_b.0.y - position_a.0.y, 0.0);
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, position_a.0.z
          ) + math::Vector3::new (0.0, -b_to_a_signum_y * sphere_a_radius, 0.0);
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true)  => {
          // nearest point is on a Z/Y parallel face of the cuboid
          let b_to_a_signum_x    = (position_a.0.x - position_b.0.x).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (b_to_a_signum_x, 0.0, 0.0);
          let nearest_b = math::Point3::from ([
            position_b.0.x + b_to_a_signum_x * cuboid_b_half_extent_x,
            position_a.0.y,
            position_a.0.z
          ]);
          let a_to_b    =
            math::Vector3::new (nearest_b.0.x - position_a.0.x, 0.0, 0.0);
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, position_a.0.z
          ) + math::Vector3::new (-b_to_a_signum_x * sphere_a_radius, 0.0, 0.0);
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // center inside X/Y bounds
          if position_a == position_b {
            let depth_x = cuboid_b_half_extent_x + sphere_a_radius;
            let depth_y = cuboid_b_half_extent_y + sphere_a_radius;
            let depth_z = cuboid_b_half_extent_z + sphere_a_radius;
            if depth_x <= depth_y && depth_x <= depth_z {
              let distance  = -depth_x;
              let half_axis = [0.5 * depth_x, 0.0, 0.0].into();
              let normal    = math::Unit3::axis_x();
              let midpoint  = position_a + half_axis -
                math::Vector3::new (sphere_a_radius, 0.0, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else if depth_y <= depth_x && depth_y <= depth_z {
              let distance = -depth_y;
              let half_axis = [0.0, 0.5 * depth_y, 0.0].into();
              let normal    = math::Unit3::axis_y();
              let midpoint  = position_a + half_axis -
                math::Vector3::new (0.0, sphere_a_radius, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else {
              debug_assert!(depth_z < depth_x);
              debug_assert!(depth_z < depth_y);
              let distance = -depth_z;
              let half_axis = [0.0, 0.0, 0.5 * depth_z].into();
              let normal    = math::Unit3::axis_z();
              let midpoint  = position_a + half_axis -
                math::Vector3::new (0.0, 0.0, sphere_a_radius);
              Proximity { distance, half_axis, midpoint, normal }
            }
          } else {
            // NB: we want the following to map zero values to positive 1.0
            // so we use f64::signum instead of math::sigvec
            let b_to_a_sigvec = (position_a - position_b).map (f64::signum);
            let corner_b      = position_b + cuboid_b.max().0 * b_to_a_sigvec;
            let depth_x       = (corner_b.0.x - position_a.0.x).abs() +
              sphere_a_radius;
            let depth_y       = (corner_b.0.y - position_a.0.y).abs() +
              sphere_a_radius;
            let depth_z       = (corner_b.0.z - position_a.0.z).abs() +
              sphere_a_radius -
              if position_a.0.z.abs() > corner_b.0.z.abs() {
                2.0 * (position_a.0.z.abs() - corner_b.0.z.abs())
              } else {
                0.0
              };

            if depth_x <= depth_y && depth_x <= depth_z {
              let distance  = -depth_x;
              let half_axis = [0.5 * b_to_a_sigvec.x * depth_x, 0.0, 0.0].into();
              let normal    = math::Unit3::unchecked (
                [b_to_a_sigvec.x, 0.0, 0.0].into());
              let midpoint  = position_a + half_axis +
                math::Vector3::new (
                  -b_to_a_sigvec.x * sphere_a_radius, 0.0, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else if depth_y <= depth_x && depth_y <= depth_z {
              let distance  = -depth_y;
              let half_axis = [0.0, 0.5 * b_to_a_sigvec.y * depth_y, 0.0].into();
              let normal    = math::Unit3::unchecked (
                [0.0, b_to_a_sigvec.y, 0.0].into());
              let midpoint  = position_a + half_axis +
                math::Vector3::new (
                  0.0, -b_to_a_sigvec.y * sphere_a_radius, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else {
              debug_assert!(depth_z < depth_x);
              debug_assert!(depth_z < depth_y);
              let nearest_a = math::Point3::new (
                position_a.0.x,
                position_a.0.y,
                position_a.0.z - b_to_a_sigvec.z * sphere_a_radius
              );
              let nearest_b = math::Point3::new (
                position_a.0.x,
                position_a.0.y,
                position_b.0.z + b_to_a_sigvec.z * cuboid_b_half_extent_z
              );
              let axis      = nearest_b - nearest_a;
              debug_assert_eq!(axis.x, 0.0);
              debug_assert_eq!(axis.y, 0.0);
              let distance  = -axis.z.abs();
              let half_axis = 0.5 * axis;
              let normal    = math::Unit3::unchecked (
                [0.0, 0.0, b_to_a_sigvec.z].into());
              let midpoint  = nearest_a + half_axis;
              Proximity { distance, half_axis, midpoint, normal }
            }
          }
        }
      }
    }
  } // end query_sphere_cuboid

  pub fn query_sphere_orthant (
    position_a : &math::Point3 <f64>, sphere_a : &shape::Sphere <f64>,
    position_b : &math::Point3 <f64>, orthant_b : &shape::Orthant
  ) -> Self {
    let radius_a    = *sphere_a.radius;
    let normal      = orthant_b.normal_axis.to_vec::<f64>();
    let normal_abs  = normal.map (f64::abs);
    let surface_sigvec = math::Vector3::new (1.0, 1.0, 1.0) - normal_abs;
    let nearest_a   = position_a -
      math::Vector3::new (radius_a, radius_a, radius_a) * normal;
    let nearest_b   =
      math::Point3 (position_a.0 * surface_sigvec + position_b.0 * normal_abs);
    let axis        = nearest_b - nearest_a;
    debug_assert_eq!(axis.sum().abs(), axis.magnitude());
    let distance    = -normal.dot (axis).signum() * axis.sum().abs();
    let half_axis   = 0.5 * axis;
    let midpoint    = nearest_a + half_axis;
    let normal      = math::Unit3::unchecked (normal);
    Proximity { distance, half_axis, midpoint, normal }
  } // end query_sphere_orthant

  pub fn query_capsule_cuboid (
    position_a : &math::Point3 <f64>, capsule_a : &shape::Capsule <f64>,
    position_b : &math::Point3 <f64>, cuboid_b  : &shape::Cuboid  <f64>
  ) -> Self {
    use math::num_traits::Zero;
    let shaft_max_a  = position_a +
      math::Vector3::new (0.0, 0.0, *capsule_a.half_height);
    let shaft_min_a  = position_a -
      math::Vector3::new (0.0, 0.0, *capsule_a.half_height);
    let cuboid_max_b = position_b + cuboid_b.max().0;
    let cuboid_min_b = position_b - cuboid_b.max().0;

    let overlap_x = cuboid_min_b.0.x < position_a.0.x &&
      position_a.0.x < cuboid_max_b.0.x;
    let overlap_y = cuboid_min_b.0.y < position_a.0.y &&
      position_a.0.y < cuboid_max_b.0.y;

    let capsule_a_radius       = *capsule_a.radius;
    let capsule_a_half_height  = *capsule_a.half_height;
    let cuboid_b_half_extent_x = *cuboid_b.half_extent_x;
    let cuboid_b_half_extent_y = *cuboid_b.half_extent_y;
    let cuboid_b_half_extent_z = *cuboid_b.half_extent_z;
    if shaft_min_a.0.z >= cuboid_max_b.0.z {
      // capsule above cuboid
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest/deepest point is a corner of the cuboid
          let b_to_a        = shaft_min_a - position_b;
          debug_assert!(!b_to_a.x.is_zero());
          debug_assert!(!b_to_a.y.is_zero());
          debug_assert!(!b_to_a.z.is_zero());
          let b_to_a_sigvec = b_to_a.map (f64::signum);
          let nearest_b     = position_b + cuboid_b.max().0 * b_to_a_sigvec;
          let a_to_b        = nearest_b - shaft_min_a;
          let nearest_a     = shaft_min_a + if !a_to_b.is_zero() {
            capsule_a_radius * a_to_b.normalized()
          } else {
            [-b_to_a_sigvec.x * capsule_a_radius, 0.0, 0.0].into()
          };
          let axis          = nearest_b - nearest_a;
          let distance      = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis     = 0.5 * axis;
          let normal        = if axis.is_zero() {
            math::Unit3::normalize (b_to_a_sigvec)
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint      = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false) => {
          // nearest/deepest point is on an edge parallel to the X axis
          let signum_y  = (shaft_min_a.0.y - position_b.0.y).signum();
          let nearest_b = math::Point3::new (
            shaft_min_a.0.x,
            position_b.0.y + signum_y * cuboid_b_half_extent_y,
            cuboid_max_b.0.z
          );
          let a_to_b    = nearest_b - shaft_min_a;
          let nearest_a = shaft_min_a + if a_to_b.is_zero() {
            -signum_y * math::Vector3::new (0.0, capsule_a_radius, 0.0)
          } else {
            capsule_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (0.0, signum_y, 1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true) => {
          // nearest/deepest point is on an edge parallel to the Y axis
          let signum_x  = (shaft_min_a.0.x - position_b.0.x).signum();
          let nearest_b = math::Point3::new (
            position_b.0.x + signum_x * cuboid_b_half_extent_x,
            shaft_min_a.0.y,
            cuboid_max_b.0.z
          );
          let a_to_b    = nearest_b - shaft_min_a;
          let nearest_a = shaft_min_a + if a_to_b.is_zero() {
            -signum_x * math::Vector3::new (capsule_a_radius, 0.0, 0.0)
          } else {
            capsule_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (signum_x, 0.0, 1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // nearest/deepest point is in the -Z direction
          let nearest_a : math::Point3 <f64> =
            shaft_min_a - math::Vector3::new (0.0, 0.0, capsule_a_radius);
          let nearest_b : math::Point3 <f64> =
            [position_a.0.x, position_a.0.y, cuboid_max_b.0.z].into();
          let axis      = nearest_b - nearest_a;
          let normal    = math::Unit3::axis_z();
          let half_axis = 0.5 * axis;
          let distance  = nearest_a.0.z - nearest_b.0.z;
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
      }
    } else if shaft_max_a.0.z <= cuboid_min_b.0.z {
      // capsule below cuboid
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest/deepest point is a corner of the cuboid
          let b_to_a        = shaft_max_a - position_b;
          debug_assert!(!b_to_a.x.is_zero());
          debug_assert!(!b_to_a.y.is_zero());
          debug_assert!(!b_to_a.z.is_zero());
          let b_to_a_sigvec = b_to_a.map (f64::signum);
          let nearest_b     = position_b + cuboid_b.max().0 * b_to_a_sigvec;
          let a_to_b        = nearest_b - shaft_max_a;
          let nearest_a     = shaft_max_a + if !a_to_b.is_zero() {
            capsule_a_radius * a_to_b.normalized()
          } else {
            [-b_to_a_sigvec.x * capsule_a_radius, 0.0, 0.0].into()
          };
          let axis          = nearest_b - nearest_a;
          let distance      = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis     = 0.5 * axis;
          let normal        = if axis.is_zero() {
            math::Unit3::normalize (b_to_a_sigvec)
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint      = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false) => {
          // nearest/deepest point is on an edge parallel to the X axis
          let signum_y  = (shaft_max_a.0.y - position_b.0.y).signum();
          let nearest_b = math::Point3::new (
            shaft_max_a.0.x,
            position_b.0.y + signum_y * cuboid_b_half_extent_y,
            cuboid_min_b.0.z
          );
          let a_to_b    = nearest_b - shaft_max_a;
          let nearest_a = shaft_max_a + if a_to_b.is_zero() {
            -signum_y * math::Vector3::new (0.0, capsule_a_radius, 0.0)
          } else {
            capsule_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (0.0, signum_y, -1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true) => {
          // nearest/deepest point is on an edge parallel to the Y axis
          let signum_x  = (shaft_max_a.0.x - position_b.0.x).signum();
          let nearest_b = math::Point3::new (
            position_b.0.x + signum_x * cuboid_b_half_extent_x,
            shaft_max_a.0.y,
            cuboid_min_b.0.z
          );
          let a_to_b    = nearest_b - shaft_max_a;
          let nearest_a = shaft_max_a + if a_to_b.is_zero() {
            -signum_x * math::Vector3::new (capsule_a_radius, 0.0, 0.0)
          } else {
            capsule_a_radius * a_to_b.normalized()
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = if axis.is_zero() {
            math::Unit3::normalize (math::Vector3::new (signum_x, 0.0, -1.0))
          } else {
            math::Unit3::unchecked_approx (-axis / distance)
          };
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // nearest/deepest point is in the -Z direction
          let nearest_a : math::Point3 <f64> =
            shaft_max_a + math::Vector3::new (0.0, 0.0, capsule_a_radius);
          let nearest_b : math::Point3 <f64> =
            [position_a.0.x, position_a.0.y, cuboid_min_b.0.z].into();
          let axis      = nearest_b - nearest_a;
          let normal    = math::Unit3::axis_z().invert();
          let half_axis = 0.5 * axis;
          let distance  = nearest_b.0.z - nearest_a.0.z;
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
      }
    } else {
      // capsule cylinder shaft overlaps on z axis
      let max_z = f64::min (shaft_max_a.0.z, cuboid_max_b.0.z);
      let min_z = f64::max (shaft_min_a.0.z, cuboid_min_b.0.z);
      debug_assert_ne!(min_z, max_z);
      let mid_z = 0.5 * (max_z + min_z);
      match (overlap_x, overlap_y) {
        (false, false) => {
          // nearest point is on a Z parallel edge of the cuboid
          let b_to_a_signum_x = (position_a.0.x - position_b.0.x).signum();
          let b_to_a_signum_y = (position_a.0.y - position_b.0.y).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (b_to_a_signum_x, b_to_a_signum_y, 0.0)
              .normalized();
          let nearest_b = math::Point3::from ([
            position_b.0.x + b_to_a_signum_x * cuboid_b_half_extent_x,
            position_b.0.y + b_to_a_signum_y * cuboid_b_half_extent_y,
            mid_z
          ]);
          let a_to_b    = math::Vector3::new (
            nearest_b.0.x - position_a.0.x,
            nearest_b.0.y - position_a.0.y,
            0.0
          );
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, mid_z
          ) + capsule_a_radius * if !a_to_b.is_zero() {
            a_to_b.normalized()
          } else {
            -b_to_a_unit_sigvec
          };
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, false)  => {
          // nearest point is on a Z/X parallel face of the cuboid
          let b_to_a_signum_y    = (position_a.0.y - position_b.0.y).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (0.0, b_to_a_signum_y, 0.0);
          let nearest_b = math::Point3::from ([
            position_a.0.x,
            position_b.0.y + b_to_a_signum_y * cuboid_b_half_extent_y,
            mid_z
          ]);
          let a_to_b    =
            math::Vector3::new (0.0, nearest_b.0.y - position_a.0.y, 0.0);
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, mid_z
          ) + math::Vector3::new (0.0, -b_to_a_signum_y * capsule_a_radius, 0.0);
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (false, true)  => {
          // nearest point is on a Z/Y parallel face of the cuboid
          let b_to_a_signum_x    = (position_a.0.x - position_b.0.x).signum();
          let b_to_a_unit_sigvec =
            math::Vector3::new (b_to_a_signum_x, 0.0, 0.0);
          let nearest_b = math::Point3::from ([
            position_b.0.x + b_to_a_signum_x * cuboid_b_half_extent_x,
            position_a.0.y,
            mid_z
          ]);
          let a_to_b    =
            math::Vector3::new (nearest_b.0.x - position_a.0.x, 0.0, 0.0);
          let nearest_a = math::Point3::new (
            position_a.0.x, position_a.0.y, mid_z
          ) + math::Vector3::new (-b_to_a_signum_x * capsule_a_radius, 0.0, 0.0);
          let axis      = nearest_b - nearest_a;
          let distance  = axis.magnitude() * if !a_to_b.is_zero() {
            a_to_b.dot (axis).signum()
          } else {
            -1.0
          };
          let half_axis = 0.5 * axis;
          let normal    = math::Unit3::unchecked_approx (if axis.is_zero() {
            b_to_a_unit_sigvec
          } else {
            -axis / distance
          });
          let midpoint  = nearest_a + half_axis;
          Proximity { distance, half_axis, midpoint, normal }
        }
        (true, true) => {
          // cylinder shaft inside X/Y bounds
          if position_a == position_b {
            let depth_x = cuboid_b_half_extent_x + capsule_a_radius;
            let depth_y = cuboid_b_half_extent_y + capsule_a_radius;
            let depth_z = cuboid_b_half_extent_z + capsule_a_half_height +
              capsule_a_radius;
            if depth_x <= depth_y && depth_x <= depth_z {
              let distance  = -depth_x;
              let half_axis = [0.5 * depth_x, 0.0, 0.0].into();
              let normal    = math::Unit3::axis_x();
              let midpoint  = position_a + half_axis -
                math::Vector3::new (capsule_a_radius, 0.0, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else if depth_y <= depth_x && depth_y <= depth_z {
              let distance = -depth_y;
              let half_axis = [0.0, 0.5 * depth_y, 0.0].into();
              let normal    = math::Unit3::axis_y();
              let midpoint  = position_a + half_axis -
                math::Vector3::new (0.0, capsule_a_radius, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else {
              debug_assert!(depth_z < depth_x);
              debug_assert!(depth_z < depth_y);
              let distance = -depth_z;
              let half_axis = [0.0, 0.0, 0.5 * depth_z].into();
              let normal    = math::Unit3::axis_z();
              let midpoint  = position_a + half_axis - math::Vector3::new (
                0.0, 0.0, capsule_a_radius + capsule_a_half_height);
              Proximity { distance, half_axis, midpoint, normal }
            }
          } else {
            // NB: we want the following to map zero values to positive 1.0
            // so we use f64::signum instead of math::sigvec
            let b_to_a_sigvec = (position_a - position_b).map (f64::signum);
            let corner_b      = position_b + cuboid_b.max().0 * b_to_a_sigvec;
            let depth_x       = (corner_b.0.x - position_a.0.x).abs() +
              capsule_a_radius;
            let depth_y       = (corner_b.0.y - position_a.0.y).abs() +
              capsule_a_radius;
            let depth_z       = (corner_b.0.z - position_a.0.z).abs() +
              capsule_a_radius + capsule_a_half_height -
              if position_a.0.z.abs() > corner_b.0.z.abs() {
                2.0 * (position_a.0.z.abs() - corner_b.0.z.abs())
              } else {
                0.0
              };

            if depth_x <= depth_y && depth_x <= depth_z {
              let distance  = -depth_x;
              let half_axis = [0.5 * b_to_a_sigvec.x * depth_x, 0.0, 0.0].into();
              let normal    = math::Unit3::unchecked (
                [b_to_a_sigvec.x, 0.0, 0.0].into());
              let midpoint  = position_a + half_axis +
                math::Vector3::new (
                  -b_to_a_sigvec.x * capsule_a_radius, 0.0, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else if depth_y <= depth_x && depth_y <= depth_z {
              let distance  = -depth_y;
              let half_axis = [0.0, 0.5 * b_to_a_sigvec.y * depth_y, 0.0].into();
              let normal    = math::Unit3::unchecked (
                [0.0, b_to_a_sigvec.y, 0.0].into());
              let midpoint  = position_a + half_axis +
                math::Vector3::new (
                  0.0, -b_to_a_sigvec.y * capsule_a_radius, 0.0);
              Proximity { distance, half_axis, midpoint, normal }
            } else {
              debug_assert!(depth_z < depth_x);
              debug_assert!(depth_z < depth_y);
              let nearest_a = math::Point3::new (
                position_a.0.x,
                position_a.0.y,
                position_a.0.z - b_to_a_sigvec.z *
                  (capsule_a_radius + capsule_a_half_height)
              );
              let nearest_b = math::Point3::new (
                position_a.0.x,
                position_a.0.y,
                position_b.0.z + b_to_a_sigvec.z * cuboid_b_half_extent_z
              );
              let axis      = nearest_b - nearest_a;
              debug_assert_eq!(axis.x, 0.0);
              debug_assert_eq!(axis.y, 0.0);
              let distance  = -axis.z.abs();
              let half_axis = 0.5 * axis;
              let normal    = math::Unit3::unchecked (
                [0.0, 0.0, b_to_a_sigvec.z].into());
              let midpoint  = nearest_a + half_axis;
              Proximity { distance, half_axis, midpoint, normal }
            }
          }
        }
      }
    }
  } // end query_capsule_cuboid

  pub fn query_capsule_orthant (
    position_a : &math::Point3 <f64>, capsule_a : &shape::Capsule <f64>,
    position_b : &math::Point3 <f64>, orthant_b : &shape::Orthant
  ) -> Self {
    let radius_a    = *capsule_a.radius;
    let half_height = *capsule_a.half_height;
    let normal      = orthant_b.normal_axis.to_vec::<f64>();
    let normal_abs  = normal.map (f64::abs);
    let surface_sigvec = math::Vector3::new (1.0, 1.0, 1.0) - normal_abs;
    let nearest_a   = position_a -
      math::Vector3::new (radius_a, radius_a, radius_a + half_height) * normal;
    let nearest_b   =
      math::Point3 (position_a.0 * surface_sigvec + position_b.0 * normal_abs);
    let axis        = nearest_b - nearest_a;
    debug_assert_eq!(axis.sum().abs(), axis.magnitude());
    let distance    = -normal.dot (axis).signum() * axis.sum().abs();
    let half_axis   = 0.5 * axis;
    let midpoint    = nearest_a + half_axis;
    let normal      = math::Unit3::unchecked (normal);
    Proximity { distance, half_axis, midpoint, normal }
  } // end query_capsule_orthant

  pub fn query_cuboid_orthant (
    position_a : &math::Point3 <f64>, cuboid_a  : &shape::Cuboid <f64>,
    position_b : &math::Point3 <f64>, orthant_b : &shape::Orthant
  ) -> Self {
    let normal      = orthant_b.normal_axis.to_vec::<f64>();
    let normal_abs  = normal.map (f64::abs);
    let surface_sigvec = math::Vector3::new (1.0, 1.0, 1.0) - normal_abs;
    let nearest_a   = position_a - cuboid_a.half_extents_vec() * normal;
    let nearest_b   =
      math::Point3 (position_a.0 * surface_sigvec + position_b.0 * normal_abs);
    let axis        = nearest_b - nearest_a;
    debug_assert_eq!(axis.sum().abs(), axis.magnitude());
    let distance    = -normal.dot (axis).signum() * axis.sum().abs();
    let half_axis   = 0.5 * axis;
    let midpoint    = nearest_a + half_axis;
    let normal      = math::Unit3::unchecked (normal);
    Proximity { distance, half_axis, midpoint, normal }
  } // end query_cuboid_orthant

} // end impl Distance

#[cfg(test)]
mod tests {
  extern crate test;
  use {std, rand};
  use rand_xorshift::XorShiftRng;
  use std::f64::consts::*;
  use super::*;
  //
  //  test_distance_query
  //
  #[test]
  fn test_distance_query() {
    use rand::{Rng, SeedableRng};
    use geometry::shape;
    use math::approx::{assert_relative_eq, assert_ulps_eq};

    //
    //  capsule v. capsule
    //

    // intersection: positions of capsules A and B are identical-- half_axis and
    // normal parallel to +X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (2.0, 3.0)).into()),
      material: component::MATERIAL_STONE,
      collidable: true
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      material: component::MATERIAL_STONE,
      collidable: true
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [ 1.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersection: cylinder shafts of A and B overlap-- half_axis and normal
    // parallel to +X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 1.0].into()),  .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, -1.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [ 1.5, 0.0,  0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, -0.5].into()
      }
    );

    // intersection: cylinder shaft max of A coincides with cylinder shaft min
    // of B
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -3.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 2.0].into()),  .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [ 1.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersection: cylinder shaft min of A coincides with cylinder shaft max of B
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 3.0].into()),  .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, -2.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [ 1.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersection: cylinder shaft of A adjacent to cylinder shaft of B
    let a = object::Static {
      position: component::Position ([-1.0, 0.0, 1.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, -1.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [-1.0, 0.0,  0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [ 0.0, 0.0, -0.5].into()
      }
    );

    // intersection: cylinder shaft of B above cylinder shaft of A
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -3.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0,  3.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [ 0.0, 0.0, -1.0].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [ 0.0, 0.0,  1.0].into()
      }
    );

    // intersection: cylinder shaft of A above cylinder shaft of B
    let a = object::Static {
      position: component::Position ([0.0, 0.0,  3.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, -3.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [ 0.0, 0.0,  1.0].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [ 0.0, 0.0, -1.0].into()
      }
    );

    // intersection: cylinder shafts of A and B are adjacent
    let a = object::Static {
      position: component::Position ([ 1.0, 0.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([-1.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // disjoint: capsule A above and to the right (+X) of capsule B
    let a = object::Static {
      position: component::Position ([ 2.0, 0.0,  5.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([-1.0, 0.0, -3.0].into()), .. b
    };

    let proximity = Proximity::query (&a, &b);
    let distance_manual = {
      let distance  = math::Vector3::new (3.0, 0.0, 3.0).magnitude() - 3.0;
      let half_axis : math::Vector3 <_> =
        0.5 * distance * math::Vector3::new (-1.0, 0.0, -1.0).normalized();
      let normal    = math::Unit3::normalize ([1.0, 0.0, 1.0].into());
      let midpoint  = math::Point3::from ([-1.0, 0.0, -1.0]) +
        *normal - half_axis;
      Proximity { distance, half_axis, midpoint, normal }
    };
    assert_eq!(proximity.distance, distance_manual.distance);
    assert_relative_eq!(proximity.half_axis, distance_manual.half_axis);
    assert_relative_eq!(*proximity.normal,   *distance_manual.normal);
    assert_relative_eq!(proximity.midpoint,  distance_manual.midpoint,
      epsilon = 2.0 * std::f64::EPSILON);

    // disjoint: cylinder shafts of A and B are adjacent
    let a = object::Static {
      position: component::Position ([ 3.0, 0.0, 3.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([-1.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 0.5, 0.0, 1.0].into()
      }
    );

    //
    //  cuboid v. cuboid
    //

    // disjoint: nearest points are corners
    let a = object::Static {
      position: component::Position ([-5.0, -5.0, -5.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 2.0, 3.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([5.0, 5.0, 5.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([3.0, 1.0, 2.0])).into()),
      .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_eq!(proximity.distance, f64::sqrt (110.0));
    assert_eq!(proximity.half_axis, [3.0, 3.5, 2.5].into());
    assert_ulps_eq!(*proximity.normal,
      -math::Vector3::new (3.0, 3.5, 2.5).normalized());
    assert_eq!(proximity.midpoint,  [-1.0, 0.5, 0.5].into());

    // disjoint: overlap on X axis only
    let a = object::Static {
      position: component::Position ([-5.0, 0.0, -5.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([5.0, 0.0, 5.0].into()), .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_eq!(proximity.distance, f64::sqrt (61.0));
    assert_eq!(proximity.half_axis, [3.0, 0.0, 2.5].into());
    assert_ulps_eq!(*proximity.normal,
      -math::Vector3::new (3.0, 0.0, 2.5).normalized());
    assert_eq!(proximity.midpoint,  [-1.0, 0.0, 0.5].into());

    //
    //  capsule v. cuboid
    //

    // disjoint: capsule nearest cuboid max corner
    let a = object::Static {
      position: component::Position ([5.0, 5.0, 3.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (SQRT_2, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_ulps_eq!(proximity.distance, 3.0 * SQRT_2);
    assert_eq!(proximity.half_axis, [-1.5, -1.5, 0.0].into());
    assert_ulps_eq!(*proximity.normal,
      math::Vector3::new (1.0, 1.0, 0.0).normalized());
    assert_eq!(proximity.midpoint,  [ 2.5, 2.5, 1.0].into());

    // disjoint: capsule nearest cuboid min corner
    let a = object::Static {
      position: component::Position ([-5.0, -5.0, -3.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (SQRT_2, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_ulps_eq!(proximity.distance, 3.0 * SQRT_2);
    assert_eq!(proximity.half_axis, [ 1.5,  1.5, 0.0].into());
    assert_ulps_eq!(*proximity.normal,
      math::Vector3::new (-1.0, -1.0, 0.0).normalized());
    assert_eq!(proximity.midpoint,  [-2.5,-2.5,-1.0].into());

    // disjoint: capsule nearest cuboid max Y edge
    let a = object::Static {
      position: component::Position ([4.0, 0.5, 4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([2.0, 2.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 2.5, 0.5, 2.0].into()
      }
    );

    // disjoint: capsule nearest cuboid min Y edge
    let a = object::Static {
      position: component::Position ([4.0, 0.5, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0,  0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 2.5, 0.5, -2.0].into()
      }
    );

    // disjoint: capsule nearest cuboid max X edge
    let a = object::Static {
      position: component::Position ([0.5, 4.0, 4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [ 0.5,  2.5, 2.0].into()
      }
    );

    // disjoint: capsule nearest cuboid min X edge
    let a = object::Static {
      position: component::Position ([0.5, 4.0, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0, -0.5,  0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [ 0.5,  2.5, -2.0].into()
      }
    );

    // disjoint: capsule A above cuboid B
    let a = object::Static {
      position: component::Position ([0.5, 0.5, 6.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0, 0.0, -0.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [ 0.5, 0.5,  2.5].into()
      }
    );

    // disjoint: capsule A below cuboid B
    let a = object::Static {
      position: component::Position ([0.5, 0.5, -6.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0, 0.0,  0.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [ 0.5, 0.5, -2.5].into()
      }
    );

    // disjoint: capsule A cylinder nearest max Z edge of cuboid B
    let a = object::Static {
      position: component::Position ([3.0, 3.0, 2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_eq!(proximity.distance, SQRT_2 - 1.0);
    assert_eq!(proximity.half_axis, [
      -0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
      -0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
       0.0
    ].into());
    assert_ulps_eq!(*proximity.normal,
      [FRAC_1_SQRT_2, FRAC_1_SQRT_2, 0.0].into());
    assert_eq!(proximity.midpoint, [
      2.0 + 0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
      2.0 + 0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
      1.0
    ].into());

    // disjoint: capsule A cylinder nearest min Z edge of cuboid B
    let a = object::Static {
      position: component::Position ([-3.0, -3.0, -2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    let proximity = Proximity::query (&a, &b);
    assert_eq!(proximity.distance, SQRT_2 - 1.0);
    assert_eq!(proximity.half_axis, [
       0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
       0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
       0.0
    ].into());
    assert_ulps_eq!(*proximity.normal,
      [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into());
    assert_eq!(proximity.midpoint, [
      -2.0 - 0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
      -2.0 - 0.5 * f64::sqrt (0.5 * (SQRT_2 - 1.0).powf (2.0)),
      -1.0
    ].into());

    // disjoint: capsule A cylinder nearest max X/Z face of cuboid B
    let a = object::Static {
      position: component::Position ([1.0, 4.0, 2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0, -0.5,  0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [ 1.0,  2.5,  1.0].into()
      }
    );

    // disjoint: capsule A cylinder nearest min X/Z face of cuboid B
    let a = object::Static {
      position: component::Position ([-1.0, -4.0, -2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),    .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.0,  0.5,  0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [-1.0, -2.5, -1.0].into()
      }
    );

    // disjoint: capsule A cylinder nearest max Y/Z face of cuboid B
    let a = object::Static {
      position: component::Position ([4.0, 1.0, 2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 2.5, 1.0, 1.0].into()
      }
    );

    // disjoint: capsule A cylinder nearest min Y/Z face of cuboid B
    let a = object::Static {
      position: component::Position ([-4.0, -1.0, -2.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),    .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.5,  0.0,  0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-2.5, -1.0, -1.0].into()
      }
    );

    // intersect: capsule cylinder shaft min equal to cuboid max corner
    let a = object::Static {
      position: component::Position ([2.0, 2.0, 4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([2.0, 2.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 1.5, 2.0, 2.0].into()
      }
    );

    // intersect: capsule cylinder shaft max equal to cuboid min corner
    let a = object::Static {
      position: component::Position ([-2.0, -2.0, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),  .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [-0.5,  0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-1.5, -2.0,-2.0].into()
      }
    );

    // intersect: capsule cylinder shaft min equal to cuboid max Y edge
    let a = object::Static {
      position: component::Position ([2.0, 1.0, 4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 1.5, 1.0, 2.0].into()
      }
    );

    // intersect: capsule cylinder shaft max equal to cuboid min Y edge
    let a = object::Static {
      position: component::Position ([-2.0, 1.0, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),  .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [-0.5, 0.0,  0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-1.5, 1.0, -2.0].into()
      }
    );

    // intersect: capsule cylinder shaft min equal to cuboid max X edge
    let a = object::Static {
      position: component::Position ([1.0, 2.0, 4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.0, 0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [ 1.0, 1.5, 2.0].into()
      }
    );

    // intersect: capsule cylinder shaft max equal to cuboid min X edge
    let a = object::Static {
      position: component::Position ([ 1.0, -2.0, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),  .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, -0.5,  0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [1.0, -1.5, -2.0].into()
      }
    );

    // intersect: capsule cylinder shaft min equal to cuboid max Z face
    let a = object::Static {
      position: component::Position ([1.0, 1.0, 4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.0, 0.0, 0.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [ 1.0, 1.0, 1.5].into()
      }
    );

    // intersect: capsule cylinder shaft max equal to cuboid min Z face
    let a = object::Static {
      position: component::Position ([-1.0, -1.0, -4.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.0,  0.0, -0.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [-1.0, -1.0, -1.5].into()
      }
    );

    // intersect: capsule and cuboid position coincide
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [ 1.5,  0.0,  0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 0.5,  0.0,  0.0].into()
      }
    );

    // intersect: capsule nearest positive X face of cuboid
    let a = object::Static {
      position: component::Position ([1.0, 0.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [ 1.0,  0.0,  0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 1.0,  0.0,  0.0].into()
      }
    );

    // intersect: capsule nearest negative X face of cuboid
    let a = object::Static {
      position: component::Position ([-1.0, 0.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [-1.0,  0.0,  0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-1.0,  0.0,  0.0].into()
      }
    );

    // intersect: capsule nearest positive Y face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, 1.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [0.0, 1.0, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0, 1.0, 0.0].into()
      }
    );

    // intersect: capsule nearest negative Y face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, -1.0, 0.0].into()), .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()), .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [0.0, -1.0, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0, -1.0, 0.0].into()
      }
    );

    // intersect: capsule nearest positive Z face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 3.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([4.0, 4.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [0.0, 0.0, 1.0].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0, 1.0].into()
      }
    );

    // intersect: capsule nearest positive Z face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 1.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([4.0, 4.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -4.0,
        half_axis: [0.0, 0.0, 2.0].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0, 0.0].into()
      }
    );

    // intersect: capsule nearest negative Z face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -3.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([4.0, 4.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -2.0,
        half_axis: [0.0, 0.0, -1.0].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0, -1.0].into()
      }
    );

    // intersect: capsule nearest negative Z face of cuboid
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -1.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([4.0, 4.0, 2.0])).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -4.0,
        half_axis: [0.0, 0.0, -2.0].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0,  0.0].into()
      }
    );

    // random queries
    let mut rng = XorShiftRng::seed_from_u64 (0);
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.5, 1.5)).into()),
      .. a
    };
    for _ in 0..100 {
      let position = component::Position ([
        rng.gen_range (-40.0..40.0),
        rng.gen_range (-40.0..40.0),
        rng.gen_range (-40.0..40.0)
      ].into());
      let bound    = component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([
          rng.gen_range (0.5..5.0),
          rng.gen_range (0.5..5.0),
          rng.gen_range (0.5..5.0)
        ])
      ).into());
      let b = object::Static {
        position, bound, material: component::MATERIAL_STONE, collidable: true
      };
      let _ = Proximity::query (&a, &b);
    }

    //
    //  capsule v. orthant
    //
    use math::SignedAxis3;

    // disjoint: capsule outside of orthant +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0, 0.0, -0.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0,  0.5].into()
      }
    );

    // disjoint: capsule outside of orthant -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0, 0.0,  0.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0, -0.5].into()
      }
    );

    // disjoint: capsule outside orthant +Y
    let a = object::Static {
      position: component::Position ([0.0, 2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // disjoint: capsule outside orthant -Y
    let a = object::Static {
      position: component::Position ([0.0, -2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // disjoint: capsule outside orthant +X
    let a = object::Static {
      position: component::Position ([2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

    // disjoint: outside orthant -X
    let a = object::Static {
      position: component::Position ([-2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [0.0, 0.0,  1.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0, -1.5].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -3.0,
        half_axis: [0.0, 0.0, -1.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0,  1.5].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal +Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal -Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal +X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: capsule and orthant position coincide; normal -X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Capsule::noisy (1.0, 2.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

    //
    //  sphere v. orthant
    //

    // disjoint: sphere outside of orthant +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  3.0,
        half_axis: [0.0, 0.0, -1.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0,  1.5].into()
      }
    );

    // disjoint: sphere outside of orthant -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  3.0,
        half_axis: [0.0, 0.0,  1.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0, -1.5].into()
      }
    );

    // disjoint: sphere outside orthant +Y
    let a = object::Static {
      position: component::Position ([0.0, 2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // disjoint: sphere outside orthant -Y
    let a = object::Static {
      position: component::Position ([0.0, -2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // disjoint: sphere outside orthant +X
    let a = object::Static {
      position: component::Position ([2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

    // disjoint: outside orthant -X
    let a = object::Static {
      position: component::Position ([-2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, 0.0,  0.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0, -0.5].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, 0.0, -0.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0,  0.5].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal +Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal -Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal +X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: sphere and orthant position coincide; normal -X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Sphere::noisy (1.0)).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

    //
    //  cuboid v. orthant
    //

    // disjoint: cuboid outside of orthant +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  3.0,
        half_axis: [0.0, 0.0, -1.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0,  1.5].into()
      }
    );

    // disjoint: cuboid outside of orthant -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, -4.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  3.0,
        half_axis: [0.0, 0.0,  1.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0, -1.5].into()
      }
    );

    // disjoint: cuboid outside orthant +Y
    let a = object::Static {
      position: component::Position ([0.0, 2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // disjoint: cuboid outside orthant -Y
    let a = object::Static {
      position: component::Position ([0.0, -2.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // disjoint: cuboid outside orthant +X
    let a = object::Static {
      position: component::Position ([2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

    // disjoint: outside orthant -X
    let a = object::Static {
      position: component::Position ([-2.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal +Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, 0.0,  0.5].into(),
        normal:    math::Unit3::axis_z(),
        midpoint:  [0.0, 0.0, -0.5].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal -Z
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegZ }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, 0.0, -0.5].into(),
        normal:    math::Unit3::axis_z().invert(),
        midpoint:  [0.0, 0.0,  0.5].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal +Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0,  0.5, 0.0].into(),
        normal:    math::Unit3::axis_y(),
        midpoint:  [0.0, -0.5, 0.0].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal -Y
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegY }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [0.0, -0.5, 0.0].into(),
        normal:    math::Unit3::axis_y().invert(),
        midpoint:  [0.0,  0.5, 0.0].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal +X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::PosX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [ 0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x(),
        midpoint:  [-0.5, 0.0, 0.0].into()
      }
    );

    // intersect: cuboid and orthant position coincide; normal -X
    let a = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Bounded::from (
        shape::Cuboid::noisy ([1.0, 1.0, 1.0])).into()),
      .. a
    };
    let b = object::Static {
      position: component::Position ([0.0, 0.0, 0.0].into()),
      bound:    component::Bound (shape::Unbounded::from (
        shape::Orthant { normal_axis: SignedAxis3::NegX }).into()),
      .. b
    };
    assert_eq!(
      Proximity::query (&a, &b),
      Proximity {
        distance:  -1.0,
        half_axis: [-0.5, 0.0, 0.0].into(),
        normal:    math::Unit3::axis_x().invert(),
        midpoint:  [ 0.5, 0.0, 0.0].into()
      }
    );

  } // end test_distance_query

  #[bench]
  /// 180ns
  fn bench_distance_query_cuboid_edges_normalize (b : &mut test::Bencher) {
    use rand::SeedableRng;
    let mut rng = XorShiftRng::seed_from_u64 (0);
    b.iter (||{
      let (position_a, cuboid_a, position_b, cuboid_b) =
        distance_query_cuboid_edge_case (&mut rng);
      let proximity = Proximity::query_cuboid_cuboid (
        &position_a, &cuboid_a, &position_b, &cuboid_b);
      assert!(proximity.distance > 0.0);
    });
  }

  /*
  #[bench]
  /// 180ns
  fn bench_distance_query_cuboid_edges_array (b : &mut test::Bencher) {
    let mut rng = rs_utils::numeric::xorshift_rng_unseeded();
    b.iter (||{
      let (position_a, cuboid_a, position_b, cuboid_b) =
        distance_query_cuboid_edge_case (&mut rng);
      let proximity = Proximity::query_cuboid_cuboid_array (
        &position_a, &cuboid_a, &position_b, &cuboid_b);
      assert!(proximity.distance > 0.0);
    });
  }

  /// 190ns
  #[bench]
  fn bench_distance_query_cuboid_edges_refactor (b : &mut test::Bencher) {
    let mut rng = rs_utils::numeric::xorshift_rng_unseeded();
    b.iter (||{
      let (position_a, cuboid_a, position_b, cuboid_b) =
        distance_query_cuboid_edge_case (&mut rng);
      let proximity = Proximity::query_cuboid_cuboid_refactor (
        &position_a, &cuboid_a, &position_b, &cuboid_b);
      assert!(proximity.distance > 0.0);
    });
  }
  */

  /// Generate an edge case for cuboid/cuboid distance benchmarks
  fn distance_query_cuboid_edge_case <RNG : rand::Rng> (rng : &mut RNG) -> (
    math::Point3 <f64>, shape::Cuboid <f64>,
    math::Point3 <f64>, shape::Cuboid <f64>
  ) {
    // overlapping range
    let base_min_a : f64 = rng.gen_range (-10.0..10.0);
    let base_max_a : f64 = rng.gen_range (base_min_a + 0.01..10.01);
    let base_min_b : f64 = rng.gen_range (-10.01..base_max_a);
    let base_max_b : f64 = if base_min_b < base_min_a {
      rng.gen_range (base_min_a + 0.01..10.02)
    } else {
      rng.gen_range (base_min_b + 0.01..10.02)
    };
    debug_assert!(base_min_a < base_max_b);
    debug_assert!(base_min_b < base_max_a);
    let base_pos_a = 0.5 * (base_min_a + base_max_a);
    let base_half_extent_a = base_max_a - base_pos_a;
    let base_pos_b = 0.5 * (base_min_b + base_max_b);
    let base_half_extent_b = base_max_b - base_pos_b;
    debug_assert!(base_half_extent_a > 0.0);
    debug_assert!(base_half_extent_b > 0.0);
    // non-overlapping ranges
    let other1_min_a = rng.gen_range (-10.0..10.0);
    let other1_max_a = rng.gen_range (other1_min_a + 0.01..10.01);
    let (other1_min_b, other1_max_b) = if rng.gen_bool (0.5) {
      // greater than
      let other1_min_b = rng.gen_range (other1_max_a + 0.01..10.02);
      let other1_max_b = rng.gen_range (other1_min_b + 0.01..10.03);
      debug_assert!(other1_max_a < other1_min_b);
      (other1_min_b, other1_max_b)
    } else {
      // less than
      let other1_min_b = rng.gen_range (-10.03..other1_min_a - 0.03);
      let other1_max_b = rng.gen_range (other1_min_b + 0.01..other1_min_a - 0.01);
      debug_assert!(other1_max_b < other1_min_a);
      (other1_min_b, other1_max_b)
    };
    let other1_pos_a = 0.5 * (other1_min_a + other1_max_a);
    let other1_half_extent_a = other1_max_a - other1_pos_a;
    let other1_pos_b = 0.5 * (other1_min_b + other1_max_b);
    let other1_half_extent_b = other1_max_b - other1_pos_b;
    debug_assert!(other1_half_extent_a > 0.0);
    debug_assert!(other1_half_extent_b > 0.0);

    let other2_min_a = rng.gen_range (-10.0..10.0);
    let other2_max_a = rng.gen_range (other2_min_a + 0.01..10.01);
    let (other2_min_b, other2_max_b) = if rng.gen_bool (0.5) {
      // greater than
      let other2_min_b = rng.gen_range (other2_max_a + 0.01..10.02);
      let other2_max_b = rng.gen_range (other2_min_b + 0.01..10.03);
      debug_assert!(other2_max_a < other2_min_b);
      (other2_min_b, other2_max_b)
    } else {
      // less than
      let other2_min_b = rng.gen_range (-10.03..other2_min_a - 0.03);
      let other2_max_b = rng.gen_range (other2_min_b + 0.01..other2_min_a - 0.01);
      debug_assert!(other2_max_b < other2_min_a);
      (other2_min_b, other2_max_b)
    };
    let other2_pos_a = 0.5 * (other2_min_a + other2_max_a);
    let other2_half_extent_a = other2_max_a - other2_pos_a;
    let other2_pos_b = 0.5 * (other2_min_b + other2_max_b);
    let other2_half_extent_b = other2_max_b - other2_pos_b;
    debug_assert!(other2_half_extent_a > 0.0);
    debug_assert!(other2_half_extent_b > 0.0);

    let axis : usize = rng.gen_range (0..3);

    if axis == 0 {
      // x
      (
        [base_pos_a, other1_pos_a, other2_pos_a].into(),
        shape::Cuboid::noisy ([
          base_half_extent_a, other1_half_extent_a, other2_half_extent_a]),
        [base_pos_b, other1_pos_b, other2_pos_b].into(),
        shape::Cuboid::noisy ([
          base_half_extent_b, other1_half_extent_b, other2_half_extent_b])
      )
    } else if axis == 1 {
      // y
      (
        [other1_pos_a, base_pos_a, other2_pos_a].into(),
        shape::Cuboid::noisy ([
          other1_half_extent_a, base_half_extent_a, other2_half_extent_a]),
        [other1_pos_b, base_pos_b, other2_pos_b].into(),
        shape::Cuboid::noisy ([
          other1_half_extent_b, base_half_extent_b, other2_half_extent_b])
      )
    } else {
      // z
      debug_assert_eq!(axis, 2);
      (
        [other1_pos_a, other2_pos_a, base_pos_a].into(),
        shape::Cuboid::noisy ([
          other1_half_extent_a, other2_half_extent_a, base_half_extent_a]),
        [other1_pos_b, other2_pos_b, base_pos_b].into(),
        shape::Cuboid::noisy ([
          other1_half_extent_b, other2_half_extent_b, base_half_extent_b])
      )
    }
  }

} // end tests
