//! A minimal particle simulation.
//!
//! # `debug_dump` feature
//!
//! Set the environment variable `LINEAR_SIM_DEBUG_DUMP=1` and before the step
//! you want to capture, raise the `collision::DEBUG_DUMP` flag to `true`. This
//! will write a binary serialization of the System state to
//! `linear-sim-<step>.dump` in the local directory. In the course of
//! development, some automatic triggers may be added in the collision pipeline
//! to raise the DEBUG_DUMP flag automatically (e.g. when collision max iters
//! has been exceeded).
//!
//! The `debug_dump_tester`
//! binary will load a target dump file and process a simulation step with the
//! deserialized System.

#![feature(test)]
#![warn(unused_extern_crates)]

pub use math_utils           as math;
pub use math_utils::geometry as geometry;

pub mod collision;
pub mod component;
pub mod constraint;
pub mod event;
pub mod force;
pub mod integrator;
pub mod object;
pub mod system;

pub use collision::Collision;
pub use constraint::Constraint;
pub use force::Force;
pub use integrator::Integrator;
pub use object::Object;
pub use system::System;
