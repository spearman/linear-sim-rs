//! Simulation force entities

use enumflags2::bitflags;
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::{math, object};

/// Force interface trait
pub trait Force {
  fn impulse <O : object::Inertial> (&self,
    object : &O, step : u64, duration : f64) -> math::Vector3 <f64>;
}

/// Bitflags for specifying certain forces
#[derive(Copy, Clone, Debug)]
#[bitflags]
#[repr(u8)]
pub enum Flag {
  Gravity = 0b_0000_0001
}

/// A gravitational field.
///
/// This is intended to imitate a gravitational field such as that found on the
/// surface of the earth where objects are small enough that their relative mass
/// is negligable, resulting in a constant acceleration for all objects.
#[cfg_attr(feature = "derive_serdes", derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Gravity {
  pub acceleration : math::Vector3 <f64>
}

impl Force for Gravity {
  fn impulse <O : object::Inertial> (&self,
    object : &O, _step : u64, duration : f64) -> math::Vector3 <f64>
  {
    duration * object.mass().mass() * self.acceleration
  }
}
