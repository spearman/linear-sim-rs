//! Simulation constraint entities

#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::{collision, component, geometry, math, object};

/// Constraint interface trait
pub trait Constraint <STATE> {
  fn evaluate_position (&self, state : &STATE) -> f64;
  fn evaluate_velocity (&self, state : &STATE) -> f64;
}

/// A planar constraint
#[cfg_attr(feature = "derive_serdes", derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Planar {
  plane : geometry::primitive::Plane3 <f64>
}

/// An inter-penetration constraint
#[derive(Clone, Debug, PartialEq)]
pub struct Penetration <'a, A, B> where
  A : object::Bounded + 'static,
  B : object::Bounded + 'static
{
  pub object_a : &'a A,
  pub object_b : &'a B
}

impl Planar {
  #[inline]
  pub fn new (
    position : math::Point3 <f64>, normal : math::Unit3 <f64>
  ) -> Self {
    let base  = position;
    let plane = geometry::primitive::Plane3 { base, normal };
    Planar { plane }
  }

  pub fn position (&self) -> math::Point3 <f64> {
    self.plane.base
  }
  pub fn normal (&self) -> math::Unit3 <f64> {
    self.plane.normal
  }
}

impl From <collision::Proximity> for Planar {
  fn from (proximity : collision::Proximity) -> Self {
    Planar::new (proximity.midpoint, proximity.normal)
  }
}

impl <O : object::Temporal> Constraint <O> for Planar {
  fn evaluate_position (&self, object : &O) -> f64 {
    let component::Position (ref position) = &object.position();
    (position.0 - self.position().0).dot (*self.normal())
  }
  fn evaluate_velocity (&self, object : &O) -> f64 {
    (object.derivatives().velocity - self.position().0)
      .dot (*self.normal())
  }
}

impl <'a, A, B> Penetration <'a, A, B> where
  A : object::Bounded, B : object::Bounded
{

  /// Apply a linear translation based on the result of a distance query on the
  /// objects.
  ///
  /// If distance query indicates the objects are not intersecting, 'None' is
  /// returned.
  pub fn resolve_position (&self) -> Option <(A, B)> {
    let distance = collision::Proximity::query (self.object_a, self.object_b);
    match distance.relation() {
      collision::proximity::Relation::Intersect => {
        let mut out_a = self.object_a.clone();
        let mut out_b = self.object_b.clone();
        let component::Position (ref position_a) = self.object_a.position();
        let component::Position (ref position_b) = self.object_b.position();
        *out_a.position_mut() =
          component::Position (position_a + distance.half_axis);
        *out_b.position_mut() =
          component::Position (position_b - distance.half_axis);
        Some ((out_a, out_b))
      }
      _ => None
    }
  }

}

// TODO: penetration constraint impl
