//! Simulation events

use enumflags2;
use derive_more::From;
#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};
use strum::Display;

use crate::{collision, component, force, math, object};

/// An input event
#[derive(Debug, PartialEq, Display, From)]
pub enum Input {
  Step,
  CreateObject  (object::Variant, Option <object::Key>),
  ModifyObject  (ObjectModify),
  DestroyObject (object::Id),
  SetGravity    (force::Gravity),
  ClearGravity
}

/// An object modification event
#[derive(Debug, PartialEq, From)]
pub enum ObjectModify {
  Dynamic (object::Key, ObjectModifyDynamic),
  Static  (object::Key, ObjectModifyStatic)
}

#[derive(Debug, PartialEq)]
pub enum ObjectModifyDynamic {
  ApplyImpulse   (math::Vector3 <f64>),
  SetForceFlags  (enumflags2::BitFlags <force::Flag>),
  SetDrag        (f64),
  SetPosition    (component::Position)
}
#[derive(Debug, PartialEq)]
pub enum ObjectModifyStatic {
  Move (math::Vector3 <f64>)
}

/// An event that results from handling an input
#[derive(Debug, PartialEq, Display, From)]
pub enum Output {
  CollisionResolve   (CollisionResolve),
  CreateObjectResult (CreateObjectResult),
  Contact            (Contact),
  Overlap            (Overlap)
}

/// A TOI collision
#[cfg_attr(feature = "derive_serdes", derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct CollisionResolve {
  pub toi               : math::Normalized <f64>,
  pub object_id_a       : object::Id,
  pub object_id_b       : object::Id,
  pub contact           : collision::contact::Colliding,
  pub impulse_normal_a  : math::Vector3 <f64>,
  pub impulse_normal_b  : math::Vector3 <f64>,
  pub impulse_tangent_a : math::Vector3 <f64>,
  pub impulse_tangent_b : math::Vector3 <f64>,
  pub pseudo_impulse_a  : math::Vector3 <f64>,
  pub pseudo_impulse_b  : math::Vector3 <f64>
}

/// A nocollide intersection
#[cfg_attr(feature = "derive_serdes", derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Overlap {
  pub object_id_a : object::Id,
  pub object_id_b : object::Id
}

/// A persistent contact
#[cfg_attr(feature = "derive_serdes", derive(Deserialize, Serialize))]
#[derive(Clone, Debug, PartialEq)]
pub struct Contact {
  pub object_id_a : object::Id,
  pub object_id_b : object::Id,
  pub contact     : collision::Contact
}

/// Events resulting from object creation
#[derive(Debug, PartialEq)]
pub enum CreateObjectResult {
  /// Success
  Created      (object::Id),
  /// Creating the object failed with an intersection.
  ///
  /// If intersections were found then the object was not successfully created.
  Intersection (Vec <(object::Id, collision::Intersection)>)
}
