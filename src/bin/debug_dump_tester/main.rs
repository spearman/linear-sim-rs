/// Load a target debug dump file and process a step

use std;
use bincode;
use env_logger;
use log;

use linear_sim::*;

fn main() {
  env_logger::builder()
    .filter_level (log::LevelFilter::Trace)
    .init();
  let filename = std::env::args().skip (1).next()
    .expect ("target dump filename is required");
  println!("loading {}...", filename);
  let mut system =
    bincode::deserialize_from::<_, System <integrator::SemiImplicitEuler>> (
      std::fs::File::open (&filename).unwrap()
    ).unwrap();
  system.handle_event (event::Input::Step);
}
