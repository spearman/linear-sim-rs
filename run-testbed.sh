#!/usr/bin/env bash

set -x

RUST_BACKTRACE=1 cargo run --example testbed --features="gl-utils env_logger" $@ #\
  #&& make -f MakefileDot testbed

exit
